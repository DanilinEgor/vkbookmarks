package com.vk.sdk.api.model;

import org.json.JSONException;
import org.json.JSONObject;

public class VKLinkArray extends VKList<VKApiLink> {
    @Override
    public VKList<VKApiLink> parse(JSONObject response) throws JSONException {
        fill(response, VKApiLink.class);
        return this;
    }
}
