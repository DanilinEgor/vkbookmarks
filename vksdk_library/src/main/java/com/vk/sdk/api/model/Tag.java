package com.vk.sdk.api.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.Random;

public class Tag implements Parcelable, Comparable {
    private static final String[] COLORS = {"#F44336", "#E91E63", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3", "#009688", "#795548", "#607D8B"};
    public int id;
    private String color;
    public String name;

    public String getColor() {
        if (color == null || color.isEmpty()) {
            color = COLORS[new Random().nextInt() % COLORS.length];
        }
        return color;
    }

    public Tag(final int id, final String name) {
        this.id = id;
        this.name = name;
        this.color = COLORS[new Random().nextInt(COLORS.length)];
    }

    protected Tag(Parcel in) {
        id = in.readInt();
        color = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(color);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Tag> CREATOR = new Creator<Tag>() {
        @Override
        public Tag createFromParcel(Parcel in) {
            return new Tag(in);
        }

        @Override
        public Tag[] newArray(int size) {
            return new Tag[size];
        }
    };

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tag tag = (Tag) o;

        return id == tag.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public int compareTo(@NonNull final Object another) {
        return ((Tag) another).id - id;
    }
}
