package xyz.egor_d.vkbookmarks.presentation.users;

import android.content.Intent;

import com.vk.sdk.api.model.VKApiUserFull;

import java.util.List;

public interface IUsersView {
    void showError(String message);

    void showUsers(List<VKApiUserFull> users);

    void setRefreshing(boolean refreshing);

//    void setBottomProgressLoading(boolean loading);

    void showSnackbar(VKApiUserFull user, boolean removed);

    void notifyPositionRemoved(int pos);

    void notifyPositionInserted(int pos);

    void sendIntent(Intent intent);
}
