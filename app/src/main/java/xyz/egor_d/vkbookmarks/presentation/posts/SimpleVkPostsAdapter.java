package xyz.egor_d.vkbookmarks.presentation.posts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiApplicationContent;
import com.vk.sdk.api.model.VKApiAudio;
import com.vk.sdk.api.model.VKApiCommunity;
import com.vk.sdk.api.model.VKApiDocument;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiNote;
import com.vk.sdk.api.model.VKApiOwner;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKApiPhotoAlbum;
import com.vk.sdk.api.model.VKApiPoll;
import com.vk.sdk.api.model.VKApiPost;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VKApiWikiPage;
import com.vk.sdk.api.model.VKAttachments;

import org.apmem.tools.layouts.FlowLayout;

import java.text.SimpleDateFormat;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;

public class SimpleVkPostsAdapter extends SimpleVkDataAdapter<VKApiPost> {
    public static class PostViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text)
        TextView text;
        @Bind(R.id.likes_count)
        TextView likesCount;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.delete)
        View delete;
        @Bind(R.id.container)
        View container;
        @Bind(R.id.audio_container)
        View audioContainer;
        @Bind(R.id.audio_name)
        TextView audioName;
        @Bind(R.id.note_container)
        View noteContainer;
        @Bind(R.id.note_name)
        TextView noteName;
        @Bind(R.id.link_container)
        View linkContainer;
        @Bind(R.id.link_name)
        TextView linkName;
        @Bind(R.id.app_container)
        View appContainer;
        @Bind(R.id.app_name)
        TextView appName;
        @Bind(R.id.poll_container)
        View pollContainer;
        @Bind(R.id.poll_name)
        TextView pollName;
        @Bind(R.id.wiki_container)
        View wikiContainer;
        @Bind(R.id.wiki_name)
        TextView wikiName;
        @Bind(R.id.document_container)
        View documentContainer;
        @Bind(R.id.document_name)
        TextView documentName;
        @Bind(R.id.owner_name)
        TextView ownerName;
        @Bind(R.id.date)
        TextView postDate;
        @Bind(R.id.owner_avatar)
        ImageView ownerAvatar;
        @Bind(R.id.video_play_icon)
        ImageView videoPlayIcon;
        @Bind(R.id.tags_container)
        FlowLayout tagsContainer;
        @Bind(R.id.add_tag)
        View addTag;

        public PostViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public View getAddTagView(final RecyclerView.ViewHolder holder) {
        return ((PostViewHolder) holder).tagsContainer.getChildAt(0);
    }

    @Override
    public RecyclerView.ViewHolder getVH(final ViewGroup parent, final int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void bindVH(final RecyclerView.ViewHolder vh, final int position) {
        PostViewHolder holder = (PostViewHolder) vh;

        holder.image.setVisibility(View.GONE);
        holder.audioContainer.setVisibility(View.GONE);
        holder.linkContainer.setVisibility(View.GONE);
        holder.appContainer.setVisibility(View.GONE);
        holder.pollContainer.setVisibility(View.GONE);
        holder.noteContainer.setVisibility(View.GONE);
        holder.documentContainer.setVisibility(View.GONE);
        holder.wikiContainer.setVisibility(View.GONE);
        holder.videoPlayIcon.setVisibility(View.GONE);
        for (int i = holder.tagsContainer.getChildCount() - 1; i >= 1; i--) {
            holder.tagsContainer.removeViewAt(i);
        }
        holder.text.setText("");

        VKApiPost post = data.get(position);

        fillTags(vh.itemView.getContext(), holder.tagsContainer, post);

        holder.postDate.setText(new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.getDefault()).format(1000 * post.date));

        while (!post.copy_history.isEmpty()) {
            post = post.copy_history.get(post.copy_history.size() - 1);
        }

        for (VKAttachments.VKApiAttachment attachment : post.attachments) {
            switch (attachment.getType()) {
                case VKAttachments.TYPE_POST:
                    VKApiPost apiPost = (VKApiPost) attachment;
                    break;
                case VKAttachments.TYPE_LINK:
                    VKApiLink link = (VKApiLink) attachment;
                    holder.linkName.setText(link.url);
                    holder.linkContainer.setVisibility(View.VISIBLE);
                    break;
                case VKAttachments.TYPE_NOTE:
                    VKApiNote note = (VKApiNote) attachment;
                    holder.noteName.setText(note.title);
                    holder.noteContainer.setVisibility(View.VISIBLE);
                    break;
                case VKAttachments.TYPE_APP:
                    VKApiApplicationContent app = (VKApiApplicationContent) attachment;
                    holder.appName.setText(app.name);
                    holder.appContainer.setVisibility(View.VISIBLE);
                    break;
                case VKAttachments.TYPE_POLL:
                    VKApiPoll poll = (VKApiPoll) attachment;
                    holder.pollName.setText(poll.question);
                    holder.pollContainer.setVisibility(View.VISIBLE);
                    break;
                case VKAttachments.TYPE_WIKI_PAGE:
                    VKApiWikiPage wiki = (VKApiWikiPage) attachment;
                    holder.wikiName.setText(wiki.title);
                    holder.wikiContainer.setVisibility(View.VISIBLE);
                    break;
                case VKAttachments.TYPE_PHOTO:
                case VKAttachments.TYPE_POSTED_PHOTO:
                    VKApiPhoto photo = (VKApiPhoto) attachment;
                    holder.image.setVisibility(View.VISIBLE);
                    Picasso.with(holder.itemView.getContext()).load(photo.photo_604).into(holder.image);
                    break;
                case VKAttachments.TYPE_ALBUM:
                    VKApiPhotoAlbum album = (VKApiPhotoAlbum) attachment;
                    holder.image.setVisibility(View.VISIBLE);
                    Picasso.with(holder.itemView.getContext()).load(album.photo.getByType('x')).into(holder.image);
                    break;
                case VKAttachments.TYPE_VIDEO:
                    VKApiVideo video = (VKApiVideo) attachment;
                    String path = video.photo_640;
                    if (path == null || path.isEmpty()) {
                        path = video.photo_320;
                    }
                    holder.image.setVisibility(View.VISIBLE);
                    holder.videoPlayIcon.setVisibility(View.VISIBLE);
                    Picasso.with(holder.itemView.getContext()).load(path).into(holder.image);
                    break;
                case VKAttachments.TYPE_AUDIO:
                    VKApiAudio audio = (VKApiAudio) attachment;
                    holder.audioName.setText(String.format("%s - %s", audio.artist, audio.title));
                    holder.audioContainer.setVisibility(View.VISIBLE);
                    break;
                case VKAttachments.TYPE_DOC:
                    VKApiDocument document = (VKApiDocument) attachment;
                    holder.documentName.setText(document.title);
                    holder.documentContainer.setVisibility(View.VISIBLE);
                    break;
            }
        }
        holder.text.setText(String.format("%s", post.text));

        holder.likesCount.setText(String.format("%s", data.get(position).likes_count));

        VKApiOwner owner = data.get(position).owner;
        if (owner != null) {
            if (owner instanceof VKApiUserFull) {
                holder.ownerName.setText(String.format("%s %s", ((VKApiUserFull) owner).first_name, ((VKApiUserFull) owner).last_name));
                Picasso.with(holder.itemView.getContext()).load(((VKApiUserFull) owner).photo_100).transform(new CropCircleTransformation()).into(holder.ownerAvatar);
            } else if (owner instanceof VKApiCommunity) {
                holder.ownerName.setText(((VKApiCommunity) owner).name);
                Picasso.with(holder.itemView.getContext()).load(((VKApiCommunity) owner).photo_100).transform(new CropCircleTransformation()).into(holder.ownerAvatar);
            }
        } else {
            holder.ownerName.setText("");
            Picasso.with(holder.itemView.getContext()).load("http://vk.com/images/camera_b.gif").transform(new CropCircleTransformation()).into(holder.ownerAvatar);
        }

        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(holder.getAdapterPosition());
            }
        });
        holder.container.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onItemClicked(holder.getAdapterPosition());
            }
        });
        holder.tagsContainer.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
    }
}
