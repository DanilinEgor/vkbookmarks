package xyz.egor_d.vkbookmarks.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import xyz.egor_d.vkbookmarks.R;

public class IntroActivity extends AppIntro {
    @Override
    public void init(@Nullable final Bundle savedInstanceState) {
        int color = getResources().getColor(R.color.colorPrimary);
        addSlide(AppIntroFragment.newInstance(getString(R.string.app_name), getString(R.string.what_is_it), R.drawable.ic_smile_white, color));
        addSlide(AppIntroFragment.newInstance(getString(R.string.storage), getString(R.string.this_is_offline_storage), R.drawable.ic_storage_white, color));
        addSlide(AppIntroFragment.newInstance(getString(R.string.cloud), getString(R.string.all_saved_to_cloud), R.drawable.ic_cloud_done_white, color));
        addSlide(AppIntroFragment.newInstance(getString(R.string.tags), getString(R.string.you_can_tag), R.drawable.ic_tag_white, color));
        showStatusBar(true);
        showSkipButton(false);
        setProgressButtonEnabled(true);
        setVibrate(false);
        ((TextView) skipButton).setText(R.string.skip);
        ((TextView) doneButton).setText(R.string.ok);
    }

    @Override
    public void onSkipPressed() {
        finish();
    }

    @Override
    public void onNextPressed() {
    }

    @Override
    public void onDonePressed() {
        finish();
    }

    @Override
    public void onSlideChanged() {
    }
}
