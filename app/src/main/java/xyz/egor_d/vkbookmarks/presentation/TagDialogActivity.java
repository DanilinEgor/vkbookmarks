package xyz.egor_d.vkbookmarks.presentation;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.egor_d.vkbookmarks.AnimUtils;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.FabDialogMorphSetup;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.TagsInfo;

public class TagDialogActivity extends AppCompatActivity {
    public static final String TAGS_LIST = "tags_list";
    public static final String CHANGED = "changed";

    @Bind(R.id.container)
    ViewGroup container;
    @Bind(R.id.tags_recycler_view)
    RecyclerView tagsRecyclerView;
    @Bind(R.id.tags_container)
    View tagsContainer;
    @Bind(R.id.empty_data_container)
    View emptyDataContainer;
    @Bind(R.id.add_tag)
    ImageView addTag;
    @Bind(R.id.add_tag_name)
    EditText addTagEditText;

    @Inject
    IStorage storage;
    private TagsInfo tagsInfo;
    private TagsAdapter adapter;
    private boolean changed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_tag);
        ((App) getApplicationContext()).getMainComponent().inject(this);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            FabDialogMorphSetup.setupSharedEelementTransitions(this, container,
                    getResources().getDimensionPixelSize(R.dimen.dialog_corners));

            if (getWindow().getSharedElementEnterTransition() != null) {
                getWindow().getSharedElementEnterTransition().addListener(new AnimUtils
                        .TransitionListenerAdapter() {
                    @Override
                    public void onTransitionEnd(Transition transition) {
//                        finishSetup();
                    }
                });
            } else {
//                finishSetup();
            }
        }

        final ArrayList<Tag> inputTags = getIntent().getParcelableArrayListExtra(TAGS_LIST);
        tagsInfo = storage.getTagsInfoOfflineSync();
        final ArrayList<Tag> allTags = (ArrayList<Tag>) tagsInfo.allTags;

        adapter = new TagsAdapter();
        tagsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter.addInputTags(inputTags);
        adapter.addAllTags(allTags);
        tagsRecyclerView.setAdapter(adapter);

        emptyDataContainer.setOnClickListener(v -> {
            createTagNameDialog(null, text -> {
                createNewTag(allTags, text);
                checkAdapterCount();
            });
        });
        adapter.setEventListener(new TagsAdapter.EventListener() {
            @Override
            public void deleteClicked(Tag tag, int pos) {
                changed = true;
                new AlertDialog.Builder(TagDialogActivity.this)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                            tagsInfo.deleteTag(tag);
                            storage.saveTagsInfoOffline(tagsInfo);
                            adapter.removeItem(tag, pos);
                            checkAdapterCount();
                        })
                        .setMessage(R.string.are_you_sure)
                        .create()
                        .show();
            }

            @Override
            public void editClicked(Tag tag, int pos) {
                changed = true;
                createTagNameDialog(tag, text -> {
                    if (!text.isEmpty()) {
                        tag.name = text;
                        tagsInfo.saveTag(tag);
                        storage.saveTagsInfoOffline(tagsInfo);
                        adapter.changeItem(tag, pos);
                    }
                });
            }
        });
        RxTextView.afterTextChangeEvents(addTagEditText)
                .subscribe(textViewAfterTextChangeEvent -> {
                    boolean enabled = textViewAfterTextChangeEvent.editable().length() != 0;
                    addTag.setEnabled(enabled);
                    addTag.setAlpha(enabled ? 1.0f : 0.5f);
                });
        addTagEditText.setOnEditorActionListener(
                (v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (addTag.isEnabled()) {
                            addTag.performClick();
                        }
                        return true;
                    }
                    return false;
                });
        addTag.setEnabled(false);
        addTag.setOnClickListener(v -> {
            createNewTag(allTags, addTagEditText.getText().toString());
            addTagEditText.setText("");
        });
        checkAdapterCount();
    }

    private void checkAdapterCount() {
        if (adapter.getItemCount() == 0) {
            emptyDataContainer.setVisibility(View.VISIBLE);
            tagsContainer.setVisibility(View.GONE);
        } else {
            emptyDataContainer.setVisibility(View.GONE);
            tagsContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }

    @OnClick(R.id.background)
    void dismiss() {
        tagsInfo.addToAllTags(adapter.getItems());
        storage.saveTagsInfoOffline(tagsInfo);
        Intent data = new Intent();
        data.putExtra(CHANGED, changed);
        data.putParcelableArrayListExtra(TAGS_LIST, adapter.getItems());
        setResult(RESULT_OK, data);
        supportFinishAfterTransition();
    }

    @OnClick(R.id.ok)
    void okClicked() {
        dismiss();
    }

    private void createNewTag(final ArrayList<Tag> allTags, final String text) {
        ArrayList<Tag> tagList = new ArrayList<>();
        int id = 1;
        if (!allTags.isEmpty()) {
            id = allTags.get(0).id + 1;
        }

        boolean dbl = true;
        while (dbl) {
            dbl = false;
            for (Tag allTag : allTags) {
                if (allTag.id == id) {
                    dbl = true;
                    ++id;
                    break;
                }
            }
        }
        tagList.add(new Tag(id, text));
        allTags.addAll(tagList);
        adapter.addInputTags(tagList);
        adapter.addAllTags(tagList);
        tagsInfo.addToAllTags(tagList);
    }

    private void createTagNameDialog(final Tag tag, TagNameDialogInterface tagNameDialogInterface) {
        View dialogView = LayoutInflater.from(this).inflate(R.layout.input_dialog_layout, null);
        final EditText editText = (EditText) dialogView.findViewById(R.id.input_dialog_edit_text);
        editText.setText(tag == null ? "" : tag.name);
        editText.setSelection(editText.getText().length());

        DialogInterface.OnClickListener okClickListener = (dialog, which) -> {
            tagNameDialogInterface.onText(editText.getText().toString());
        };

        Dialog dialog =
                new AlertDialog.Builder(this)
                        .setView(dialogView)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.ok, okClickListener)
                        .setTitle(R.string.input_tag_name_hint)
                        .create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    private interface TagNameDialogInterface {
        void onText(String text);
    }
}
