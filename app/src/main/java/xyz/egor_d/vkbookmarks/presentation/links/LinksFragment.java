package xyz.egor_d.vkbookmarks.presentation.links;

import android.content.Intent;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.vk.sdk.api.model.VKApiLink;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.presentation.MainActivity;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BaseFragment;

public class LinksFragment extends BaseFragment implements ILinksView, SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.users_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.users_recycler_view)
    RecyclerView mRecyclerView;
    private LinksAdapter mAdapter;

    @Inject
    LinksPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vk_data, container, false);
        ButterKnife.bind(this, view);

        mAdapter = new LinksAdapter();
        mAdapter.setEventListener(new LinksAdapter.EventListener() {
            @Override
            public void onDeleteClicked(final int pos) {
                getPresenter().itemDeleteClicked(pos);
            }

            @Override
            public void onItemClicked(final int position) {
                getPresenter().itemClicked(position);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        if (!supportsViewElevation()) {
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z1)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable.list_divider_h), true));

        return view;
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    @Override
    public void onRefresh() {
        getPresenter().refresh(true);
    }

    @Override
    public void showSnackbar(VKApiLink link, boolean removed) {
        Snackbar
                .make(
                        mRecyclerView,
                        String.format("%s %s", link.title, getString(R.string.deleted_link)),
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }

    @Override
    public void notifyPositionRemoved(final int pos) {
        mAdapter.notifyItemRemoved(pos);
    }

    @Override
    public void notifyPositionInserted(final int pos) {
        mAdapter.notifyItemInserted(pos);
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected LinksPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLinks(final List<VKApiLink> links) {
        mAdapter.setLinks(links);
    }

    @Override
    public void setRefreshing(final boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

    @Override
    public void sendIntent(final Intent intent) {
        getActivity().startActivity(intent);
    }
}
