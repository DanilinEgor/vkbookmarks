package xyz.egor_d.vkbookmarks.presentation.users;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiUserFull;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.R;

public class SimpleUsersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int ITEM = 1;
    private final int PROGRESS = 2;
//    private boolean bottomLoading = true;

    private List<VKApiUserFull> users = new ArrayList<>();
    private EventListener eventListener;

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.container)
        View container;

        public UserViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface EventListener {
        void onDeleteClicked(int pos);

        void onItemClicked(int position);
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

//    public void setBottomLoading(final boolean bottomLoading) {
//        this.bottomLoading = bottomLoading;
//        Timber.v(bottomLoading + " " + users.size() + "");
//        if (bottomLoading) {
//            notifyItemInserted(users.size());
//        } else {
//            notifyItemRemoved(users.size());
//        }
//    }

//    public boolean isBottomLoading() {
//        return bottomLoading;
//    }

    @Override
    public int getItemViewType(final int position) {
        return position == users.size() ? PROGRESS : ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        if (viewType == ITEM) {
            return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
        } else {
            return new RecyclerView.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_bottom_loading, parent, false)) {
            };
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder vh, final int position) {
        if (vh.getItemViewType() == ITEM) {
            UserViewHolder holder = (UserViewHolder) vh;
            VKApiUserFull user = users.get(position);

            holder.name.setText(String.format("%s %s %s", position + "", user.first_name, user.last_name));
            Picasso.with(holder.itemView.getContext()).load(user.photo_100).transform(new CropCircleTransformation()).into(holder.avatar);

            holder.delete.setOnClickListener(v -> {
                if (eventListener != null) {
                    eventListener.onDeleteClicked(position);
                }
            });

            holder.container.setOnClickListener(v -> {
                if (eventListener != null) {
                    eventListener.onItemClicked(position);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return users.size();// + (bottomLoading ? 1 : 0);
    }

    @Override
    public long getItemId(final int position) {
        if (position < users.size()) {
            return users.get(position).getId();
        }
        return 0;
    }

    public void setUsers(final List<VKApiUserFull> users) {
        Timber.v(users.size() + "");
        this.users = users;
        notifyItemRangeChanged(0, getItemCount());
    }
}
