package xyz.egor_d.vkbookmarks.presentation.links;

import android.content.Intent;

import com.vk.sdk.api.model.VKApiLink;

import java.util.List;

public interface ILinksView {
    void showError(String message);

    void showLinks(List<VKApiLink> links);

    void setRefreshing(boolean refreshing);

    void showSnackbar(VKApiLink link, boolean removed);

    void notifyPositionRemoved(int pos);

    void notifyPositionInserted(int pos);

    void sendIntent(Intent intent);
}
