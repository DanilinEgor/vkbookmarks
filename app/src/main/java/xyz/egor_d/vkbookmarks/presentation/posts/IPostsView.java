package xyz.egor_d.vkbookmarks.presentation.posts;

import com.vk.sdk.api.model.VKApiPost;

import java.util.List;

public interface IPostsView {
    void showError(String message);

    void showPosts(List<VKApiPost> posts);

    void setRefreshing(boolean refreshing);

    void showSnackbar(VKApiPost post, boolean removed);

    void notifyPositionRemoved(int pos);

    void notifyPositionInserted(int pos);
}
