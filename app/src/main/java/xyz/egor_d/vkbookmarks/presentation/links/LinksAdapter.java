package xyz.egor_d.vkbookmarks.presentation.links;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiLink;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class LinksAdapter extends RecyclerView.Adapter<LinksAdapter.LinkViewHolder> {
    List<VKApiLink> links = new ArrayList<>();
    EventListener eventListener;

    public static class LinkViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.description)
        TextView description;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.container)
        View container;

        public LinkViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface EventListener {
        void onDeleteClicked(int pos);

        void onItemClicked(int position);
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public LinkViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new LinkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_link, parent, false));
    }

    @Override
    public void onBindViewHolder(final LinkViewHolder holder, final int position) {
        VKApiLink link = links.get(position);

        holder.title.setText(String.format("%s", link.title));
        holder.description.setText(String.format("%s", link.description));
        Picasso.with(holder.itemView.getContext()).load(link.photo_100).into(holder.avatar);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(position);
            }
        });
        holder.container.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onItemClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return links.size();
    }

    public void setLinks(final List<VKApiLink> links) {
        this.links = links;
        notifyDataSetChanged();
    }
}
