package xyz.egor_d.vkbookmarks.presentation.login;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class LoginPresenter extends BasePresenter<ILoginView> {
    @Override
    public void onStart() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void itemClicked(final int position) {

    }

    @Override
    public void itemDeleteClicked(final int position) {

    }

    @Override
    public void undoPressed() {

    }

    @Override
    public void requestUpdate(final int offset) {

    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }
}
