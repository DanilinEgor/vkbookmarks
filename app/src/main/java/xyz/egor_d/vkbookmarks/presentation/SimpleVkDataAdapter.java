package xyz.egor_d.vkbookmarks.presentation;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiModel;

import java.util.ArrayList;
import java.util.List;

import xyz.egor_d.vkbookmarks.R;

public abstract class SimpleVkDataAdapter<T extends VKApiModel> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected List<T> data = new ArrayList<>();
    protected EventListener eventListener;

    public void addItem(final int pos, final T item) {
        data.add(pos, item);
        notifyItemInserted(pos);
    }

    public void removeItem(final int pos) {
        data.remove(pos);
        notifyItemRemoved(pos);
    }

    public void changeItem(final int pos, final T item) {
        data.set(pos, item);
        notifyItemChanged(pos);
    }

    public interface EventListener {
        void onDeleteClicked(int pos);

        void onItemClicked(int position);

        void onAddTagClicked(int position);
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    public abstract View getAddTagView(RecyclerView.ViewHolder holder);

    public abstract RecyclerView.ViewHolder getVH(final ViewGroup parent, final int viewType);

    public abstract void bindVH(RecyclerView.ViewHolder holder, final int position);

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return getVH(parent, viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder vh, final int position) {
        bindVH(vh, position);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(final List<T> data) {
        int prevSize = this.data.size();
        this.data.clear();
        this.data.addAll(data);
        if (this.data.size() < prevSize) {
            notifyItemRangeRemoved(this.data.size(), prevSize - this.data.size());
        }
        notifyItemRangeChanged(0, getItemCount());
    }

    protected void fillTags(Context context, ViewGroup parent, final T item) {
        if (item.appTags != null) {
            for (Tag tag : item.appTags) {
                TextView text = (TextView) LayoutInflater.from(context).inflate(R.layout.tag, parent, false);
                Drawable background = text.getBackground();
                ((GradientDrawable) background).setColor(Color.parseColor(tag.getColor()));
                text.setBackground(background);
                text.setText(tag.name);
                parent.addView(text);
            }
        }
    }
}
