package xyz.egor_d.vkbookmarks.presentation;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

public abstract class BasePresenter<View> {
    View view;

    public void setView(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }

    public abstract void onStart();

    public abstract void onStop();

    public abstract void itemClicked(final int position);

    public abstract void itemDeleteClicked(final int position);

    public abstract void undoPressed();

    public abstract void requestUpdate(final int offset);

    public abstract void addTagClicked(final int position);

    public abstract void tagsReturned(final ArrayList<Tag> tags, boolean somethingChanged);

    public ArrayList<Tag> getFilterTags() {
        return null;
    }

    public void setFilterTags(final ArrayList<Tag> filterTags) {
    }
}
