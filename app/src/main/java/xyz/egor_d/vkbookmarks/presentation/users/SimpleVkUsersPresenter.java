package xyz.egor_d.vkbookmarks.presentation.users;

import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiUserFull;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.vkbookmarks.data.TagsInfo;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataPresenter;
import xyz.egor_d.vkbookmarks.data.IStorage;

public class SimpleVkUsersPresenter extends SimpleVkDataPresenter<VKApiUserFull> {
    @Inject
    public SimpleVkUsersPresenter(final IStorage storage) {
        super(storage);
    }

    @Override
    protected Map<Integer, List<Tag>> getTagsMap(final TagsInfo tagsInfo) {
        return tagsInfo.userTagsMap;
    }

    @Override
    public Observable<List<VKApiUserFull>> getFaveOffline() {
        return storage.getFaveUsersOffline();
    }

    @Override
    public Observable<List<VKApiUserFull>> getFaveOnline(final int offset, final boolean force) {
        return storage.getFaveUsersOnline(offset, force);
    }

    @Override
    protected Observable<Boolean> removeFromFave() {
        return storage.removeUserFromFave(lastItem.getId());
    }

    @Override
    public void itemClicked(final int position) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Locale.getDefault(), "vk://profile/%d", data.get(position).getId())));
        getView().sendIntent(intent);
    }
}
