package xyz.egor_d.vkbookmarks.presentation.photos;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiPhoto;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class PhotosPresenter extends BasePresenter<IPhotosView> {
    private final IStorage storage;
    private final CompositeSubscription subscription = new CompositeSubscription();
    private List<VKApiPhoto> photos = new ArrayList<>();
    private int lastPos;
    private VKApiPhoto lastPhoto;
    private Subscription removePhotoFromFaveSubscription;

    @Inject
    public PhotosPresenter(IStorage storage) {
        this.storage = storage;
    }

    @Override
    public void onStart() {
        refresh(false);
    }

    @Override
    public void onStop() {
        storage.setFavePhotosOffline(photos);
        subscription.clear();
    }

    @Override
    public void itemClicked(final int position) {

    }

    public void refresh(boolean force) {
        Observable<List<VKApiPhoto>> o1;
        if (force) {
            o1 = storage.getFavePhotosOnline(0, true);
        } else {
            o1 = storage.getFavePhotosOffline().concatWith(storage.getFavePhotosOnline(0, false));
        }
        subscription.add(o1
                .map(photos -> {
                    this.photos.clear();
                    this.photos.addAll(photos);
                    return this.photos;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        photos -> getView().showPhotos(photos),
                        throwable -> {
                            Timber.e("Error:", throwable);
                            getView().showError(throwable.getMessage());
                            getView().setRefreshing(false);
                        },
                        () -> getView().setRefreshing(false)
                ));
    }

    public void itemDeleteClicked(int pos) {
        lastPos = pos;
        lastPhoto = photos.get(pos);

        photos.remove(lastPhoto);

        removePhotoFromFaveSubscription =
                storage
                        .removeLike("photo", lastPhoto.owner_id, lastPhoto.id)
                        .delaySubscription(2, TimeUnit.SECONDS)
                        .subscribe(
                                success -> {
                                },
                                throwable -> {
                                }
                        );

        subscription.add(removePhotoFromFaveSubscription);

        getView().notifyPositionRemoved(pos);
        getView().showSnackbar(lastPhoto, true);
    }

    public void undoPressed() {
        if (removePhotoFromFaveSubscription != null) {
            removePhotoFromFaveSubscription.unsubscribe();
        }
        photos.add(lastPos, lastPhoto);
        getView().notifyPositionInserted(lastPos);
    }

    @Override
    public void requestUpdate(final int offset) {

    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }
}
