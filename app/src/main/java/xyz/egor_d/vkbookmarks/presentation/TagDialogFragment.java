package xyz.egor_d.vkbookmarks.presentation;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.TagsInfo;

public class TagDialogFragment extends DialogFragment {
    public static final String TAGS_LIST = "tags_list";

    @Inject
    IStorage storage;

    @Bind(R.id.tags_recycler_view)
    RecyclerView tagsRecyclerView;
    @Bind(R.id.add_tag)
    ImageView addTag;
    @Bind(R.id.tags_container)
    View tagsContainer;
    @Bind(R.id.empty_data_container)
    View emptyDataContainer;
    @Bind(R.id.add_tag_name)
    EditText addTagEditText;

    private TagsAdapter adapter;
    private TagsInfo tagsInfo;

    public static TagDialogFragment newInstance() {
        return newInstance(new ArrayList<>());
    }

    public static TagDialogFragment newInstance(ArrayList<Tag> inputTags) {
        Bundle args = new Bundle();
        if (inputTags == null) {
            inputTags = new ArrayList<>();
        }
        args.putParcelableArrayList(TAGS_LIST, inputTags);
        TagDialogFragment fragment = new TagDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);

        final ArrayList<Tag> inputTags = getArguments().getParcelableArrayList(TAGS_LIST);
        tagsInfo = storage.getTagsInfoOfflineSync();
        final ArrayList<Tag> allTags = (ArrayList<Tag>) tagsInfo.allTags;

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_tag, null);

        ButterKnife.bind(this, view);

        emptyDataContainer.setOnClickListener(v -> {
            createTagNameDialog(null, text -> {
                createNewTag(allTags, text);
                checkAdapterCount();
            });
        });

        tagsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new TagsAdapter();
        adapter.addInputTags(inputTags);
        adapter.addAllTags(allTags);
        adapter.setEventListener(new TagsAdapter.EventListener() {
            @Override
            public void deleteClicked(Tag tag, int pos) {
                new AlertDialog.Builder(getActivity())
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.ok, (dialog, which) -> {
                            tagsInfo.deleteTag(tag);
                            storage.saveTagsInfoOffline(tagsInfo);
                            adapter.removeItem(tag, pos);
                            checkAdapterCount();
                        })
                        .setMessage(R.string.are_you_sure)
                        .create()
                        .show();
            }

            @Override
            public void editClicked(Tag tag, int pos) {
                createTagNameDialog(tag, text -> {
                    if (!text.isEmpty()) {
                        tag.name = text;
                        tagsInfo.saveTag(tag);
                        storage.saveTagsInfoOffline(tagsInfo);
                        adapter.changeItem(tag, pos);
                    }
                });
            }
        });
        tagsRecyclerView.setAdapter(adapter);

        RxTextView.afterTextChangeEvents(addTagEditText)
                .subscribe(textViewAfterTextChangeEvent -> {
                    boolean enabled = textViewAfterTextChangeEvent.editable().length() != 0;
                    addTag.setEnabled(enabled);
                    addTag.setAlpha(enabled ? 1.0f : 0.5f);
                });
        addTagEditText.setOnEditorActionListener(
                (v, actionId, event) -> {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        if (addTag.isEnabled()) {
                            addTag.performClick();
                        }
                        return true;
                    }
                    return false;
                });
        addTag.setEnabled(false);
        addTag.setOnClickListener(v -> {
            createNewTag(allTags, addTagEditText.getText().toString());
            addTagEditText.setText("");
        });

        checkAdapterCount();

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton("Ok", (dialog, which) -> {
                    dismiss();
                })
                .create();
    }

    private void createNewTag(final ArrayList<Tag> allTags, final String text) {
        ArrayList<Tag> tagList = new ArrayList<>();
        int id = 1;
        if (!allTags.isEmpty()) {
            id = allTags.get(0).id + 1;
        }

        boolean dbl = true;
        while (dbl) {
            dbl = false;
            for (Tag allTag : allTags) {
                if (allTag.id == id) {
                    dbl = true;
                    ++id;
                    break;
                }
            }
        }
        tagList.add(new Tag(id, text));
        allTags.addAll(tagList);
        adapter.addInputTags(tagList);
        adapter.addAllTags(tagList);
        tagsInfo.addToAllTags(tagList);
    }

    private void createTagNameDialog(final Tag tag, TagNameDialogInterface tagNameDialogInterface) {
        View dialogView = LayoutInflater.from(getActivity()).inflate(R.layout.input_dialog_layout, null);
        final EditText editText = (EditText) dialogView.findViewById(R.id.input_dialog_edit_text);
        editText.setText(tag == null ? "" : tag.name);
        editText.setSelection(editText.getText().length());

        DialogInterface.OnClickListener okClickListener = (dialog, which) -> {
            tagNameDialogInterface.onText(editText.getText().toString());
        };

        Dialog dialog =
                new AlertDialog.Builder(getActivity())
                        .setView(dialogView)
                        .setNegativeButton(R.string.cancel, null)
                        .setPositiveButton(R.string.ok, okClickListener)
                        .setTitle(R.string.input_tag_name_hint)
                        .create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
    }

    private void checkAdapterCount() {
        if (adapter.getItemCount() == 0) {
            emptyDataContainer.setVisibility(View.VISIBLE);
            tagsContainer.setVisibility(View.GONE);
        } else {
            emptyDataContainer.setVisibility(View.GONE);
            tagsContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        tagsInfo.addToAllTags(adapter.getItems());
        storage.saveTagsInfoOffline(tagsInfo);
        Intent intent = new Intent();
        intent.putParcelableArrayListExtra(TAGS_LIST, adapter.getItems());
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
    }

    private interface TagNameDialogInterface {
        void onText(String text);
    }
}
