package xyz.egor_d.vkbookmarks.presentation.links;

import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiLink;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.vkbookmarks.data.TagsInfo;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataPresenter;
import xyz.egor_d.vkbookmarks.data.IStorage;

public class SimpleVkLinksPresenter extends SimpleVkDataPresenter<VKApiLink> {
    @Inject
    public SimpleVkLinksPresenter(final IStorage storage) {
        super(storage);
    }

    @Override
    protected Map<Integer, List<Tag>> getTagsMap(final TagsInfo tagsInfo) {
        return tagsInfo.linkTagsMap;
    }

    @Override
    public Observable<List<VKApiLink>> getFaveOffline() {
        return storage.getFaveLinksOffline();
    }

    @Override
    public Observable<List<VKApiLink>> getFaveOnline(final int offset, final boolean force) {
        return storage.getFaveLinksOnline(offset, force);
    }

    @Override
    protected Observable<Boolean> removeFromFave() {
        return storage.removeLinkFromFave(lastItem.id);
    }

    @Override
    public void itemClicked(final int position) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.get(position).url));
        getView().sendIntent(intent);
    }
}
