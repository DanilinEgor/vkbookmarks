package xyz.egor_d.vkbookmarks.presentation.users;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vk.sdk.api.model.VKApiUserFull;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;
import rx.android.schedulers.AndroidSchedulers;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.presentation.MainActivity;
import xyz.egor_d.vkbookmarks.PaginationTool;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BaseFragment;

public class UsersFragment extends BaseFragment implements IUsersView, SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.users_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.users_recycler_view)
    RecyclerView mRecyclerView;

    private SimpleVkUsersAdapter mAdapter;

    @Inject
    UsersPresenter presenter;

    //    private RecyclerViewTouchActionGuardManager mRecyclerViewTouchActionGuardManager;
//    private RecyclerViewSwipeManager mRecyclerViewSwipeManager;
//    private RecyclerView.Adapter mWrappedAdapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected UsersPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showUsers(final List<VKApiUserFull> users) {
        mAdapter.setData(users);
//        List<AdapterCommand> commands = commandsCalculator.diff(users);
//        commandProcessor.execute(commands);
    }

    @Override
    public void setRefreshing(final boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }

//    @Override
//    public void setBottomProgressLoading(final boolean loading) {
//        mAdapter.setBottomLoading(loading);
//    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vk_data, container, false);
        ButterKnife.bind(this, view);

//        mRecyclerViewTouchActionGuardManager = new RecyclerViewTouchActionGuardManager();
//        mRecyclerViewTouchActionGuardManager.setInterceptVerticalScrollingWhileAnimationRunning(true);
//        mRecyclerViewTouchActionGuardManager.setEnabled(true);
//
//        mRecyclerViewSwipeManager = new RecyclerViewSwipeManager();
        mAdapter = new SimpleVkUsersAdapter();
        mAdapter.setHasStableIds(true);
//        mAdapter.setEventListener(new SimpleVkDataAdapter.EventListener() {
//            @Override
//            public void onDeleteClicked(final int pos) {
//                getPresenter().itemDeleteClicked(pos);
//            }
//
//            @Override
//            public void onItemClicked(final int position) {
//                getPresenter().itemClicked(position);
//            }
//        });

//        mAdapter = new UsersAdapter();
//        mAdapter.setEventListener(new UsersAdapter.EventListener() {
//            @Override
//            public void onItemSwipedLeft(final int position) {
//                getPresenter().itemSwipedLeft(position);
//            }
//
//            @Override
//            public void onItemSwipedRight(final int position) {
//                getPresenter().itemSwipedRight(position);
//            }
//
//            @Override
//            public void onItemViewClicked(final View v, final boolean pinned) {
//
//            }
//        });

//        mWrappedAdapter = mRecyclerViewSwipeManager.createWrappedAdapter(mAdapter);

//        final GeneralItemAnimator animator = new SwipeDismissItemAnimator();
//        animator.setSupportsChangeAnimations(false);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        mLayoutManager = new LinearLayoutManager(getActivity());

        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setAdapter(mWrappedAdapter);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setItemAnimator(new SlideInUpAnimator());
//        mRecyclerView.setItemAnimator(animator);
//
//        if (!supportsViewElevation()) {
//            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z1)));
//        }
//        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable.list_divider_h), true));

//        mRecyclerViewTouchActionGuardManager.attachRecyclerView(mRecyclerView);
//        mRecyclerViewSwipeManager.attachRecyclerView(mRecyclerView);

        PaginationTool
                .buildPagingObservable(mRecyclerView)
                .getPagingObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offset -> {
                    getPresenter().requestUpdate(offset);// - (mAdapter.isBottomLoading() ? 1 : 0));
                }, throwable -> {
                    showError(throwable.getMessage());
                });


        return view;
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    @Override
    public void onDestroyView() {
//        if (mRecyclerViewSwipeManager != null) {
//            mRecyclerViewSwipeManager.release();
//            mRecyclerViewSwipeManager = null;
//        }
//
//        if (mRecyclerViewTouchActionGuardManager != null) {
//            mRecyclerViewTouchActionGuardManager.release();
//            mRecyclerViewTouchActionGuardManager = null;
//        }

        if (mRecyclerView != null) {
            mRecyclerView.setItemAnimator(null);
            mRecyclerView.setAdapter(null);
            mRecyclerView = null;
        }

//        if (mWrappedAdapter != null) {
//            WrapperAdapterUtils.releaseAll(mWrappedAdapter);
//            mWrappedAdapter = null;
//        }
        mAdapter = null;
        mLayoutManager = null;

        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        getPresenter().requestUpdate(0);
    }

    @Override
    public void showSnackbar(VKApiUserFull user, boolean removed) {
        String action = getString(removed ?
                (user.sex == 1 ? R.string.deleted_from_fave_f : R.string.deleted_from_fave_m) :
                (user.sex == 1 ? R.string.added_to_ignore_f : R.string.added_to_ignore_m));
        Snackbar
                .make(
                        mRecyclerView,
                        String.format("%s %s %s", user.first_name, user.last_name, action),
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }

    @Override
    public void notifyPositionRemoved(final int pos) {
        mAdapter.notifyItemRemoved(pos);
    }

    @Override
    public void notifyPositionInserted(final int pos) {
        mAdapter.notifyItemInserted(pos);
    }

    @Override
    public void sendIntent(final Intent intent) {
        getActivity().startActivity(intent);
    }
}
