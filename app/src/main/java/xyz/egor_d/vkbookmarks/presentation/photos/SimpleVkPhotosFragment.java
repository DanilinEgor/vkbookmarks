package xyz.egor_d.vkbookmarks.presentation.photos;

import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.vk.sdk.api.model.VKApiPhoto;

import javax.inject.Inject;

import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;
import xyz.egor_d.vkbookmarks.presentation.VkDataFragment;

public class SimpleVkPhotosFragment extends VkDataFragment<VKApiPhoto> {
    @Inject
    SimpleVkPhotosPresenter presenter;

    @Override
    public SimpleVkDataAdapter<VKApiPhoto> getAdapter() {
        return new SimpleVkPhotosAdapter();
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return new GridLayoutManager(getActivity(), 2);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSnackbar(final VKApiPhoto photo, final boolean removed) {
    }

    protected int getStartAnimColor() {
        return Color.parseColor("#60FFFFFF");
    }
}
