package xyz.egor_d.vkbookmarks.presentation.photos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiPhoto;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;

public class SimpleVkPhotosAdapter extends SimpleVkDataAdapter<VKApiPhoto> {
    public static class PhotoViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.tags_container)
        ViewGroup tagsContainer;

        public PhotoViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public View getAddTagView(final RecyclerView.ViewHolder holder) {
        return ((PhotoViewHolder) holder).tagsContainer.getChildAt(0);
    }

    @Override
    public RecyclerView.ViewHolder getVH(final ViewGroup parent, final int viewType) {
        return new PhotoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void bindVH(final RecyclerView.ViewHolder vh, final int position) {
        PhotoViewHolder holder = (PhotoViewHolder) vh;
        VKApiPhoto photo = data.get(position);

        for (int i = holder.tagsContainer.getChildCount() - 1; i >= 1; i--) {
            holder.tagsContainer.removeViewAt(i);
        }

        fillTags(vh.itemView.getContext(), holder.tagsContainer, photo);
        Picasso.with(holder.itemView.getContext()).load(photo.photo_604).into(holder.image);
        holder.image.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onItemClicked(holder.getAdapterPosition());
            }
        });
        holder.tagsContainer.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
    }
}
