package xyz.egor_d.vkbookmarks.presentation.users;

import android.support.design.widget.Snackbar;

import com.vk.sdk.api.model.VKApiUserFull;

import javax.inject.Inject;

import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;
import xyz.egor_d.vkbookmarks.presentation.VkDataFragment;

public class SimpleVkUsersFragment extends VkDataFragment<VKApiUserFull> {
    @Inject
    SimpleVkUsersPresenter presenter;

    @Override
    public SimpleVkDataAdapter<VKApiUserFull> getAdapter() {
        return new SimpleVkUsersAdapter();
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSnackbar(final VKApiUserFull user, final boolean removed) {
        String action = getString(removed ?
                (user.sex == 1 ? R.string.deleted_from_fave_f : R.string.deleted_from_fave_m) :
                (user.sex == 1 ? R.string.added_to_ignore_f : R.string.added_to_ignore_m));
        Snackbar
                .make(
                        mRecyclerView,
                        String.format("%s %s %s", user.first_name, user.last_name, action),
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }
}
