package xyz.egor_d.vkbookmarks.presentation;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.TagViewHolder> {
    private ArrayList<Tag> allTags = new ArrayList<>();
    private ArrayList<Tag> inputTags = new ArrayList<>();
    private EventListener eventListener;
    private boolean editOptionsVisible = true;

    @Override
    public TagViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag, parent, false);
        return new TagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TagViewHolder holder, final int position) {
        final Tag tag = allTags.get(position);
        holder.tagName.setText(tag.name);
        Drawable background = holder.tagName.getBackground();
        ((GradientDrawable) background).setColor(Color.parseColor(tag.getColor()));
        holder.tagName.setBackground(background);
        holder.checkBox.setChecked(inputTags.contains(tag));
        holder.checkBox.setOnClickListener(v -> {
            if (holder.checkBox.isChecked()) {
                inputTags.add(tag);
            } else {
                inputTags.remove(tag);
            }
        });
        holder.container.setOnClickListener(v -> holder.checkBox.performClick());
        holder.editTag.setVisibility(editOptionsVisible ? View.VISIBLE : View.GONE);
        holder.deleteTag.setVisibility(editOptionsVisible ? View.VISIBLE : View.GONE);
        holder.editTag.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.editClicked(tag, holder.getAdapterPosition());
            }
        });
        holder.deleteTag.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.deleteClicked(tag, holder.getAdapterPosition());
            }
        });
    }

    @Override
    public int getItemCount() {
        return allTags.size();
    }

    public void addInputTags(final ArrayList<Tag> tags) {
        this.inputTags.addAll(tags);
    }

    public void clearInputTags() {
        this.inputTags.clear();
        notifyDataSetChanged();
    }

    public ArrayList<Tag> getItems() {
        return inputTags;
    }

    public void addAllTags(final ArrayList<Tag> allTags) {
        this.allTags.addAll(0, allTags);
        notifyItemRangeInserted(0, allTags.size());
    }

    public void changeItem(final Tag tag, final int pos) {
        allTags.set(pos, tag);
        if (inputTags.indexOf(tag) != -1) {
            inputTags.get(inputTags.indexOf(tag)).name = tag.name;
        }
        notifyItemChanged(pos);
    }

    public void removeItem(final Tag tag, int pos) {
        allTags.remove(tag);
        inputTags.remove(tag);
        notifyItemRemoved(pos);
    }

    public void hideEditOptions() {
        editOptionsVisible = false;
    }

    public interface EventListener {
        void deleteClicked(Tag tag, int pos);

        void editClicked(Tag tag, int pos);

    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    class TagViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tag_checkbox)
        CheckBox checkBox;
        @Bind(R.id.tag_name)
        TextView tagName;
        @Bind(R.id.container)
        View container;
        @Bind(R.id.edit_tag)
        View editTag;
        @Bind(R.id.delete_tag)
        View deleteTag;

        public TagViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
