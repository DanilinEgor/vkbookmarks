package xyz.egor_d.vkbookmarks.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.main.MainFragment;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_TAGS = 3;
//    @Bind(R.id.toolbar)
//    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
//        boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
//        if (isFirstStart) {
//            Intent i = new Intent(MainActivity.this, IntroActivity.class);
//            startActivity(i);
//            getPrefs.edit().putBoolean("firstStart", true).apply();
//        }

        setContentView(R.layout.activity_main);
//        ButterKnife.bind(this);
//        setSupportActionBar(toolbar);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().setDisplayUseLogoEnabled(true);
//            getSupportActionBar().setDisplayShowHomeEnabled(true);
//            getSupportActionBar().setLogo(R.drawable.animated_logo);
//        }
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainFragment(), "Main").commit();
        }
    }

    public void setFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.slide_in_right,
                        R.anim.slide_out_left,
                        R.anim.slide_in_left,
                        R.anim.slide_out_right
                )
                .addToBackStack(fragment.getTag())
                .replace(R.id.container, fragment)
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
                break;
            case R.id.manage_tags:
                TagDialogFragment fragment = TagDialogFragment.newInstance();
                fragment.setTargetFragment(getSupportFragmentManager().findFragmentByTag("Main"), REQUEST_TAGS);
                fragment.show(getSupportFragmentManager(), fragment.getClass().getName());
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
