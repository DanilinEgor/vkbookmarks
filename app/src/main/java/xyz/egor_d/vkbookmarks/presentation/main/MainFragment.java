package xyz.egor_d.vkbookmarks.presentation.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BaseFragment;
import xyz.egor_d.vkbookmarks.presentation.MainActivity;
import xyz.egor_d.vkbookmarks.presentation.TagDialogFragment;


public class MainFragment extends BaseFragment implements IMainView, MainAdapter.IMainClickListener {
    @Inject
    MainPresenter presenter;
    @Bind(R.id.main_recycler_view)
    RecyclerView mainRecyclerView;

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected MainPresenter getPresenter() {
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);
        MainAdapter adapter = new MainAdapter();
        mainRecyclerView.setAdapter(adapter);
        mainRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter.setMainClickListener(this);
        return view;
    }

    @Override
    public void showError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setFragment(final Fragment fragment) {
        ((MainActivity) getActivity()).setFragment(fragment);
    }

    @OnClick(R.id.clear)
    public void clearClicked() {
        getPresenter().clearStorageInfo();
    }

    @Override
    public void onClicked(MainAdapter.MainMenuEnum type) {
        getPresenter().onClicked(type);
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case MainActivity.REQUEST_TAGS:
                    ArrayList<Tag> tags = data.getParcelableArrayListExtra(TagDialogFragment.TAGS_LIST);
                    getPresenter().tagsReturned(tags, false);
                    break;
            }
        }
    }
}
