package xyz.egor_d.vkbookmarks.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.widget.Toast;

import com.mikepenz.aboutlibraries.Libs;
import com.mikepenz.aboutlibraries.LibsBuilder;
import com.vk.sdk.VKSdk;

import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.domain.Preferences;
import xyz.egor_d.vkbookmarks.presentation.login.LoginActivity;

public class SettingsActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(final Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            findPreference("intro").setOnPreferenceClickListener(preference -> {
                startActivity(new Intent(getActivity(), IntroActivity.class));
                return true;
            });
            findPreference("license").setOnPreferenceClickListener(preference -> {
                new LibsBuilder()
                        .withActivityStyle(Libs.ActivityStyle.LIGHT_DARK_TOOLBAR)
                        .start(getActivity());
                return true;
            });
            findPreference("clear_cache").setOnPreferenceClickListener(preference -> {
                new Preferences(getActivity()).clear();
                Toast.makeText(getActivity(), R.string.cache_cleared, Toast.LENGTH_SHORT).show();
                return true;
            });
            findPreference("logout").setOnPreferenceClickListener(preference -> {
                new Preferences(getActivity()).clear();
                VKSdk.logout();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                return true;
            });
        }
    }
}
