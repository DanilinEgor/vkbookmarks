package xyz.egor_d.vkbookmarks.presentation.users;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemConstants;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultAction;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.action.SwipeResultActionRemoveItem;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiUserFull;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class UsersAdapter
        extends RecyclerView.Adapter<UsersAdapter.UserViewHolder>
        implements SwipeableItemAdapter<UsersAdapter.UserViewHolder> {
    List<VKApiUserFull> users = new ArrayList<>();
    private EventListener mEventListener;

    public interface EventListener {
        void onItemSwipedLeft(int position);

        void onItemSwipedRight(int position);

        void onItemViewClicked(View v, boolean pinned);
    }

    public static class UserViewHolder extends AbstractSwipeableItemViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.container)
        LinearLayout container;

        public UserViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @Override
        public View getSwipeableContainerView() {
            return container;
        }
    }

    public UsersAdapter() {
        setHasStableIds(true);
    }

    @Override
    public long getItemId(final int position) {
        return users.get(position).id;
    }

    @Override
    public UsersAdapter.UserViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new UsersAdapter.UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void onBindViewHolder(final UserViewHolder holder, final int position) {
        VKApiUserFull user = users.get(position);

        holder.name.setText(String.format("%s %s %s", user.id, user.first_name, user.last_name));
        Picasso.with(holder.itemView.getContext()).load(user.photo_200).into(holder.avatar);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setEventListener(EventListener eventListener) {
        mEventListener = eventListener;
    }

    @Override
    public SwipeResultAction onSwipeItem(final UserViewHolder holder, final int position, final int result) {
        switch (result) {
            case SwipeableItemConstants.RESULT_SWIPED_RIGHT:
                return new SwipeRightResultAction(this, position);
            case SwipeableItemConstants.RESULT_SWIPED_LEFT:
                return new SwipeLeftResultAction(this, position);
        }
        return null;
    }

    @Override
    public int onGetSwipeReactionType(final UserViewHolder holder, final int position, final int x, final int y) {
        return SwipeableItemConstants.REACTION_CAN_SWIPE_BOTH_H;
    }

    @Override
    public void onSetSwipeBackground(final UserViewHolder holder, final int position, final int type) {
        int bgRes = 0;
        switch (type) {
            case SwipeableItemConstants.DRAWABLE_SWIPE_NEUTRAL_BACKGROUND:
                bgRes = R.drawable.bg_swipe_item_neutral;
                break;
            case SwipeableItemConstants.DRAWABLE_SWIPE_LEFT_BACKGROUND:
                bgRes = R.drawable.bg_swipe_item_left;
                break;
            case SwipeableItemConstants.DRAWABLE_SWIPE_RIGHT_BACKGROUND:
                bgRes = R.drawable.bg_swipe_item_right;
                break;
        }

        holder.itemView.setBackgroundResource(bgRes);
    }

    public void setUsers(final List<VKApiUserFull> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    private static class SwipeLeftResultAction extends SwipeResultActionRemoveItem {
        private UsersAdapter mAdapter;
        private final int mPosition;

        SwipeLeftResultAction(UsersAdapter adapter, int position) {
            mAdapter = adapter;
            mPosition = position;
        }

        @Override
        protected void onPerformAction() {
        }

        @Override
        protected void onSlideAnimationEnd() {
            if (mAdapter.mEventListener != null) {
                mAdapter.mEventListener.onItemSwipedLeft(mPosition);
            }
        }

        @Override
        protected void onCleanUp() {
            mAdapter = null;
        }
    }

    private static class SwipeRightResultAction extends SwipeResultActionRemoveItem {
        private UsersAdapter mAdapter;
        private final int mPosition;

        SwipeRightResultAction(UsersAdapter adapter, int position) {
            mAdapter = adapter;
            mPosition = position;
        }

        @Override
        protected void onPerformAction() {
        }

        @Override
        protected void onSlideAnimationEnd() {
            if (mAdapter.mEventListener != null) {
                mAdapter.mEventListener.onItemSwipedRight(mPosition);
            }
        }

        @Override
        protected void onCleanUp() {
            mAdapter = null;
        }
    }
}
