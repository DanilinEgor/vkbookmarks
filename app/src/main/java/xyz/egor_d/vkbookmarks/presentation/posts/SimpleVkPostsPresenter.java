package xyz.egor_d.vkbookmarks.presentation.posts;

import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiOwner;
import com.vk.sdk.api.model.VKApiPost;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.TagsInfo;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataPresenter;

public class SimpleVkPostsPresenter extends SimpleVkDataPresenter<VKApiPost> {
    private Observable.Transformer<List<VKApiPost>, List<VKApiPost>> ownerTransformer =
            listObservable ->
                    listObservable.map(vkApiPosts -> {
                        List<Integer> ids = new ArrayList<>();
                        for (VKApiPost vkApiPost : vkApiPosts) {
                            ids.add(vkApiPost.from_id);
                        }
                        return Observable.combineLatest(
                                Observable.just(vkApiPosts),
                                storage.getInfo(ids),
                                (resPosts, vkApiOwners) -> {
                                    for (VKApiPost post : resPosts) {
                                        for (VKApiOwner owner : vkApiOwners) {
                                            if (owner.getId() == post.from_id) {
                                                post.owner = owner;
                                                continue;
                                            }
                                            if (owner.getId() == -post.from_id) {
                                                post.owner = owner;
                                            }
                                        }
                                    }
                                    return resPosts;
                                }
                        ).toBlocking().first();
                    });

    @Inject
    public SimpleVkPostsPresenter(final IStorage storage) {
        super(storage);
    }

    @Override
    protected Map<Integer, List<Tag>> getTagsMap(final TagsInfo tagsInfo) {
        return tagsInfo.postTagsMap;
    }

    @Override
    public Observable<List<VKApiPost>> getFaveOffline() {
        return storage
                .getFavePostsOffline()
                .compose(ownerTransformer);
    }

    @Override
    public Observable<List<VKApiPost>> getFaveOnline(final int offset, final boolean force) {
        return storage
                .getFavePostsOnline(offset, force)
                .compose(ownerTransformer);
    }

    @Override
    protected Observable<Boolean> removeFromFave() {
        return storage
                .removeLike("post", lastItem.from_id, lastItem.id)
                .map(likes -> true);
    }

    @Override
    public void itemClicked(final int position) {
        VKApiPost post = data.get(position);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Locale.getDefault(), "https://vk.com/wall%d_%d", post.from_id, post.id)));
        getView().sendIntent(intent);
    }
}
