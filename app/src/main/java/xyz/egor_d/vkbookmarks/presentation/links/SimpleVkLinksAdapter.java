package xyz.egor_d.vkbookmarks.presentation.links;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiLink;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;

public class SimpleVkLinksAdapter extends SimpleVkDataAdapter<VKApiLink> {
    public static class LinkViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.description)
        TextView description;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.container)
        View container;
        @Bind(R.id.tags_container)
        LinearLayout tagsContainer;
        @Bind(R.id.add_tag)
        View addTag;

        public LinkViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public View getAddTagView(RecyclerView.ViewHolder holder) {
        return ((LinkViewHolder) holder).addTag;
    }

    @Override
    public RecyclerView.ViewHolder getVH(final ViewGroup parent, final int viewType) {
        return new LinkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_link, parent, false));
    }

    @Override
    public void bindVH(final RecyclerView.ViewHolder vh, final int position) {
        LinkViewHolder holder = (LinkViewHolder) vh;
        VKApiLink link = data.get(position);

        holder.title.setText(String.format("%s", link.title));
        holder.description.setText(String.format("%s", link.description));

        for (int i = holder.tagsContainer.getChildCount() - 1; i >= 0; i--) {
            holder.tagsContainer.removeViewAt(i);
        }

        fillTags(vh.itemView.getContext(), holder.tagsContainer, link);

        Picasso.with(holder.itemView.getContext()).load(link.photo_100).into(holder.avatar);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(holder.getAdapterPosition());
            }
        });
        holder.container.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onItemClicked(holder.getAdapterPosition());
            }
        });
        holder.tagsContainer.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
        holder.addTag.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
    }
}
