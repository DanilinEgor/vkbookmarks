package xyz.egor_d.vkbookmarks.presentation.videos;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiVideo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class VideosPresenter extends BasePresenter<IVideosView> {
    private final IStorage storage;
    private final CompositeSubscription subscription = new CompositeSubscription();
    private List<VKApiVideo> videos = new ArrayList<>();
    private int lastPos;
    private VKApiVideo lastVideo;
    private Subscription removeLinkFromFaveSubscription;

    @Inject
    public VideosPresenter(IStorage storage) {
        this.storage = storage;
    }

    @Override
    public void onStart() {
        refresh(false);
    }

    @Override
    public void onStop() {
        storage.setFaveVideosOffline(videos);
        subscription.clear();
    }

    @Override
    public void itemClicked(final int position) {

    }

    public void refresh(boolean force) {
        Observable<List<VKApiVideo>> o1;
        if (force) {
            o1 = storage.getFaveVideosOnline(0, true);
        } else {
            o1 = storage.getFaveVideosOffline().concatWith(storage.getFaveVideosOnline(0, false));
        }
        subscription.add(o1
                .map(vkApiLinks -> {
                    videos.clear();
                    videos.addAll(vkApiLinks);
                    return videos;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        vkApiLinks -> getView().showVideos(vkApiLinks),
                        throwable -> {
                            Timber.e("Error:", throwable);
                            getView().showError(throwable.getMessage());
                            getView().setRefreshing(false);
                        },
                        () -> getView().setRefreshing(false)
                ));
    }

    public void itemDeleteClicked(int pos) {
        lastPos = pos;
        lastVideo = videos.get(pos);

        videos.remove(lastVideo);

        removeLinkFromFaveSubscription =
                storage
                        .removeLike("video", lastVideo.owner_id, lastVideo.id)
                        .delaySubscription(2, TimeUnit.SECONDS)
                        .subscribe(
                                success -> {
                                },
                                throwable -> {
                                }
                        );

        subscription.add(removeLinkFromFaveSubscription);

        getView().notifyPositionRemoved(pos);
        getView().showSnackbar(lastVideo, true);
    }

    public void undoPressed() {
        if (removeLinkFromFaveSubscription != null) {
            removeLinkFromFaveSubscription.unsubscribe();
        }
        videos.add(lastPos, lastVideo);
        getView().notifyPositionInserted(lastPos);
    }

    @Override
    public void requestUpdate(final int offset) {

    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }
}
