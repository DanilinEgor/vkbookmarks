package xyz.egor_d.vkbookmarks.presentation;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Transition;
import android.view.View;
import android.view.ViewGroup;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.egor_d.vkbookmarks.AnimUtils;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.FabDialogMorphSetup;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.TagsInfo;

public class FilterDialogActivity extends AppCompatActivity {
    public static final String TAGS_LIST = "tags_list";

    @Bind(R.id.container)
    ViewGroup container;
    @Bind(R.id.tags_recycler_view)
    RecyclerView tagsRecyclerView;
    @Bind(R.id.tags_container)
    View tagsContainer;
    @Bind(R.id.empty_data_container)
    View emptyDataContainer;

    @Inject
    IStorage storage;
    private TagsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ((App) getApplicationContext()).getMainComponent().inject(this);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            FabDialogMorphSetup.setupSharedEelementTransitions(this, container,
                    getResources().getDimensionPixelSize(R.dimen.dialog_corners));

            if (getWindow().getSharedElementEnterTransition() != null) {
                getWindow().getSharedElementEnterTransition().addListener(new AnimUtils
                        .TransitionListenerAdapter() {
                    @Override
                    public void onTransitionEnd(Transition transition) {
//                        finishSetup();
                    }
                });
            } else {
//                finishSetup();
            }
        }

        final ArrayList<Tag> inputTags = getIntent().getParcelableArrayListExtra(TAGS_LIST);
        final TagsInfo tagsInfo = storage.getTagsInfoOfflineSync();
        final ArrayList<Tag> allTags = (ArrayList<Tag>) tagsInfo.allTags;

        adapter = new TagsAdapter();
        adapter.hideEditOptions();
        tagsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        adapter.addInputTags(inputTags);
        adapter.addAllTags(allTags);
        tagsRecyclerView.setAdapter(adapter);

        checkAdapterCount();
    }

    private void checkAdapterCount() {
        if (adapter.getItemCount() == 0) {
            emptyDataContainer.setVisibility(View.VISIBLE);
            tagsContainer.setVisibility(View.GONE);
        } else {
            emptyDataContainer.setVisibility(View.GONE);
            tagsContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }

    @OnClick(R.id.background)
    void dismiss() {
        Intent data = new Intent();
        data.putParcelableArrayListExtra(TAGS_LIST, adapter.getItems());
        setResult(RESULT_OK, data);
        supportFinishAfterTransition();
    }

    @OnClick(R.id.clear_tag)
    void clearTagsClicked() {
        adapter.clearInputTags();
    }

    @OnClick(R.id.ok)
    void okClicked() {
        dismiss();
    }
}