package xyz.egor_d.vkbookmarks.presentation.main;

import android.support.v4.app.Fragment;

public interface IMainView {
    void showError(String message);

    void setFragment(Fragment fragment);
}
