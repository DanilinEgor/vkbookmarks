package xyz.egor_d.vkbookmarks.presentation.photos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiPhoto;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class PhotosAdapter extends RecyclerView.Adapter<PhotosAdapter.LinkViewHolder> {
    List<VKApiPhoto> photos = new ArrayList<>();
    EventListener eventListener;

    public static class LinkViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;

        public LinkViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface EventListener {
        void onDeleteClicked(int pos);
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public LinkViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new LinkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false));
    }

    @Override
    public void onBindViewHolder(final LinkViewHolder holder, final int position) {
        VKApiPhoto photo = photos.get(position);

        Picasso.with(holder.itemView.getContext()).load(photo.photo_130).into(holder.avatar);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    public void setPhotos(final List<VKApiPhoto> photos) {
        this.photos = photos;
        notifyDataSetChanged();
    }
}
