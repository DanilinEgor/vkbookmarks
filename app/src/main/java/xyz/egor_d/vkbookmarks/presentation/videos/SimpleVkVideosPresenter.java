package xyz.egor_d.vkbookmarks.presentation.videos;


import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiVideo;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.TagsInfo;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataPresenter;

public class SimpleVkVideosPresenter extends SimpleVkDataPresenter<VKApiVideo> {
    @Inject
    public SimpleVkVideosPresenter(final IStorage storage) {
        super(storage);
    }

    @Override
    protected Map<Integer, List<Tag>> getTagsMap(final TagsInfo tagsInfo) {
        return tagsInfo.videoTagsMap;
    }

    @Override
    public Observable<List<VKApiVideo>> getFaveOffline() {
        return storage.getFaveVideosOffline();
    }

    @Override
    public Observable<List<VKApiVideo>> getFaveOnline(final int offset, final boolean force) {
        return storage.getFaveVideosOnline(offset, force);
    }

    @Override
    protected Observable<Boolean> removeFromFave() {
        return storage
                .removeLike("video", lastItem.owner_id, lastItem.id)
                .map(likes -> true);
    }

    @Override
    public void itemClicked(final int position) {
        VKApiVideo video = data.get(position);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Locale.getDefault(), "https://vk.com/video%d_%d", video.owner_id, video.id)));
        getView().sendIntent(intent);
    }
}
