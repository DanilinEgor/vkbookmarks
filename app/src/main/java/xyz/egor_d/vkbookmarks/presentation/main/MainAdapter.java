package xyz.egor_d.vkbookmarks.presentation.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {
    private final List<MainMenuEnum> list = Arrays.asList(
            MainMenuEnum.PHOTOS,
            MainMenuEnum.VIDEOS,
            MainMenuEnum.POSTS,
            MainMenuEnum.USERS,
            MainMenuEnum.LINKS
    );
    private IMainClickListener mainClickListener;

    @Override
    public MainViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, final int position) {
        holder.bind(list.get(position), mainClickListener);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void setMainClickListener(final IMainClickListener mainClickListener) {
        this.mainClickListener = mainClickListener;
    }

    public static class MainViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_main_card)
        View card;
        @Bind(R.id.item_main_text)
        TextView text;
        @Bind(R.id.item_main_image)
        ImageView image;
        private Picasso picasso;

        public void bind(MainMenuEnum type, IMainClickListener listener) {
            text.setText(type.getTextId());
            picasso.load(type.getImageId()).into(image);
            card.setOnClickListener(v -> {
                if (listener != null) listener.onClicked(type);
            });
        }

        public MainViewHolder(final View itemView) {
            super(itemView);
            picasso = Picasso.with(itemView.getContext());
            ButterKnife.bind(this, itemView);
        }
    }

    public interface IMainClickListener {
        void onClicked(MainMenuEnum type);
    }

    public enum MainMenuEnum {
        POSTS {
            @Override
            public Integer getImageId() {
                return R.drawable.image_post;
            }

            @Override
            public Integer getTextId() {
                return R.string.posts;
            }
        },
        PHOTOS {
            @Override
            public Integer getImageId() {
                return R.drawable.image_photo;
            }

            @Override
            public Integer getTextId() {
                return R.string.photos;
            }
        },
        VIDEOS {
            @Override
            public Integer getImageId() {
                return R.drawable.image_video;
            }

            @Override
            public Integer getTextId() {
                return R.string.videos;
            }
        },
        LINKS {
            @Override
            public Integer getImageId() {
                return R.drawable.image_link;
            }

            @Override
            public Integer getTextId() {
                return R.string.links;
            }
        },
        USERS {
            @Override
            public Integer getImageId() {
                return R.drawable.image_user;
            }

            @Override
            public Integer getTextId() {
                return R.string.users;
            }
        };

        public abstract Integer getImageId();

        public abstract Integer getTextId();

    }
}
