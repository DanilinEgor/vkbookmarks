package xyz.egor_d.vkbookmarks.presentation.posts;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.vk.sdk.api.model.VKApiPost;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostViewHolder> {
    List<VKApiPost> posts = new ArrayList<>();
    EventListener eventListener;

    public static class PostViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;

        public PostViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface EventListener {
        void onDeleteClicked(int pos);
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public PostViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new PostViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void onBindViewHolder(final PostViewHolder holder, final int position) {
        VKApiPost post = posts.get(position);

        holder.name.setText(String.format("%s", post.text));
//        Picasso.with(holder.itemView.getContext()).load(post.).into(holder.avatar);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

    public void setPosts(final List<VKApiPost> posts) {
        this.posts = posts;
        notifyDataSetChanged();
    }
}
