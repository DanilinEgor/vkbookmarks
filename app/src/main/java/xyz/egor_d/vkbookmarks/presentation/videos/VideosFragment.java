package xyz.egor_d.vkbookmarks.presentation.videos;

import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.vk.sdk.api.model.VKApiVideo;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.presentation.MainActivity;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BaseFragment;

public class VideosFragment extends BaseFragment implements IVideosView, SwipeRefreshLayout.OnRefreshListener {
    @Bind(R.id.users_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.users_recycler_view)
    RecyclerView mRecyclerView;
    private VideosAdapter mAdapter;

    @Inject
    VideosPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vk_data, container, false);
        ButterKnife.bind(this, view);

        mAdapter = new VideosAdapter();
        mAdapter.setEventListener(pos -> {
            getPresenter().itemDeleteClicked(pos);
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mAdapter);

        if (!supportsViewElevation()) {
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z1)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable.list_divider_h), true));

        return view;
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    @Override
    public void onRefresh() {
        getPresenter().refresh(true);
    }

    @Override
    public void showSnackbar(VKApiVideo video, boolean removed) {
        Snackbar
                .make(
                        mRecyclerView,
                        String.format("%s %s", video.title, getString(R.string.deleted_link)),
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }

    @Override
    public void notifyPositionRemoved(final int pos) {
        mAdapter.notifyItemRemoved(pos);
    }

    @Override
    public void notifyPositionInserted(final int pos) {
        mAdapter.notifyItemInserted(pos);
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected VideosPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showVideos(final List<VKApiVideo> links) {
        mAdapter.setVideos(links);
    }

    @Override
    public void setRefreshing(final boolean refreshing) {
        swipeRefreshLayout.setRefreshing(refreshing);
    }
}
