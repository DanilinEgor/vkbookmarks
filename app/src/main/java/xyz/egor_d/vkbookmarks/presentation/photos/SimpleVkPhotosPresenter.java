package xyz.egor_d.vkbookmarks.presentation.photos;

import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiPhoto;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.inject.Inject;

import rx.Observable;
import xyz.egor_d.vkbookmarks.data.TagsInfo;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataPresenter;
import xyz.egor_d.vkbookmarks.data.IStorage;

public class SimpleVkPhotosPresenter extends SimpleVkDataPresenter<VKApiPhoto> {
    @Inject
    public SimpleVkPhotosPresenter(final IStorage storage) {
        super(storage);
    }

    @Override
    protected Map<Integer, List<Tag>> getTagsMap(final TagsInfo tagsInfo) {
        return tagsInfo.photoTagsMap;
    }

    @Override
    public Observable<List<VKApiPhoto>> getFaveOffline() {
        return storage.getFavePhotosOffline();
    }

    @Override
    public Observable<List<VKApiPhoto>> getFaveOnline(final int offset, final boolean force) {
        return storage.getFavePhotosOnline(offset, force);
    }

    @Override
    protected Observable<Boolean> removeFromFave() {
        return storage
                .removeLike("photo", lastItem.owner_id, lastItem.id)
                .map(likes -> true);
    }

    @Override
    public void itemClicked(final int position) {
        VKApiPhoto photo = data.get(position);
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Locale.getDefault(), "https://vk.com/photo%d_%d", photo.owner_id, photo.id)));
        getView().sendIntent(intent);
    }
}
