package xyz.egor_d.vkbookmarks.presentation.users;

import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiUserFull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.domain.Preferences;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class UsersPresenter extends BasePresenter<IUsersView> {
    private final IStorage storage;
    //    private final Preferences preferences;
    //    private StorageInfo info;
    private final CompositeSubscription subscription = new CompositeSubscription();
    private final List<VKApiUserFull> users = new ArrayList<>();
    //    private Set<Integer> ignoredIds = new TreeSet<>();
//    private Set<Integer> newIgnoredIds = new TreeSet<>();
//    private Set<Integer> deletedIds = new TreeSet<>();
    private int lastPos;
    private VKApiUserFull lastUser;
    private Subscription removeUserFromFaveSubscription;

    @Inject
    public UsersPresenter(IStorage storage, Preferences preferences) {
        this.storage = storage;
//        this.preferences = preferences;
    }

    @Override
    public void onStart() {
//        deletedIds = new Gson().fromJson(preferences.getString(Preferences.DELETED_IDS_KEY, ""), new TypeToken<TreeSet<Integer>>() {
//        }.getType());
//        if (deletedIds == null) {
//            deletedIds = new TreeSet<>();
//        }
        getView().setRefreshing(true);
        subscription.add(
                storage
                        .getFaveUsersOffline()
                        .concatWith(
                                storage.getFaveUsersOnline(0, false)
                        )
                        .subscribeOn(Schedulers.io())
                        .flatMap(userFulls -> {
                            users.clear();
                            users.addAll(userFulls);
                            return Observable.just(users);
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                userFulls -> {
                                    getView().showUsers(userFulls);
                                },
                                throwable -> {
                                    Timber.e("Error:", throwable);
                                    getView().showError(throwable.getMessage());
                                    getView().setRefreshing(false);
                                },
                                () -> getView().setRefreshing(false)
                        )
        );
    }

    @Override
    public void onStop() {
//        storage.setFaveUsersOffline(users);
//        if (!newIgnoredIds.isEmpty()) {
//            ignoredIds.addAll(newIgnoredIds);
//            info.ignoredMap.put(StorageInfo.USERS_IGNORED_KEY, ignoredIds);
//            preferences.putString(Storage.IGNORED_KEY, new Gson().toJson(info));
//            preferences.putBoolean(Preferences.STORAGE_SYNCED, false);
//        }
//        if (!deletedIds.isEmpty()) {
//            preferences.putBoolean(Preferences.DELETED_SYNCED, false);
//            preferences.putString(Preferences.DELETED_IDS_KEY, new Gson().toJson(deletedIds));
//        }
        subscription.clear();
    }


    public void requestUpdate(int offset) {
//        getView().setBottomProgressLoading(true);
        getView().setRefreshing(true);
        subscription.add(
                //TODO: запросит юзеров начиная с offset, переделать потом на подписку на storage и запрос обновления
                storage
                        .getFaveUsersOnline(offset, true)
                        .observeOn(AndroidSchedulers.mainThread())
                        .flatMap(userFulls -> {
                            synchronized (users) {
//                                int prevCount = users.size();
                                users.clear();
                                users.addAll(userFulls);
//                                if (users.size() >= prevCount && users.size() - prevCount < PaginationTool.DEFAULT_LIMIT) {
//                                    getView().setBottomProgressLoading(false);
//                                }
                            }
                            return Observable.just(users);
                        })
                        .subscribe(
                                userFulls -> {
                                    getView().showUsers(userFulls);
                                },
                                throwable -> {
                                    Timber.e("Error:", throwable);
                                    getView().showError(throwable.getMessage());
                                    getView().setRefreshing(false);
                                },
                                () -> getView().setRefreshing(false)
                        )
        );
    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }

    public void itemSwipedLeft(int pos) {
        lastPos = pos;
        lastUser = users.get(pos);

//        newIgnoredIds.add(lastUser.id);
//        ignoredIds.add(lastUser.id);
        users.remove(lastUser);

        getView().notifyPositionRemoved(pos);
        getView().showSnackbar(lastUser, false);
    }

    @Override
    public void itemDeleteClicked(final int pos) {
        lastPos = pos;
        lastUser = users.get(pos);

        users.remove(lastUser);
//        deletedIds.add(lastUser.id);

        removeUserFromFaveSubscription =
                storage
                        .removeUserFromFave(lastUser.id)
                        .delaySubscription(2, TimeUnit.SECONDS)
                        .subscribe(
                                success -> {
                                },//deletedIds.remove(lastUser.id),
                                throwable -> {
                                }
                        );

        subscription.add(removeUserFromFaveSubscription);

        getView().notifyPositionRemoved(pos);
        getView().showSnackbar(lastUser, true);
    }

    public void undoPressed() {
        if (removeUserFromFaveSubscription != null) {
            removeUserFromFaveSubscription.unsubscribe();
        }
//        deletedIds.remove(lastUser.id);
//        ignoredIds.remove(lastUser.id);
//        newIgnoredIds.remove(lastUser.id);
        users.add(lastPos, lastUser);
        getView().notifyPositionInserted(lastPos);
    }

    public void itemClicked(final int position) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(Locale.getDefault(), "vk://profile/%d", users.get(position).getId())));
        getView().sendIntent(intent);
    }

//    public Observable<List<VKApiUserFull>> onNext(final int offset, boolean force) {
//        Observable<List<VKApiUserFull>> o1;
//        Observable<StorageInfo> o2;
//        if (force) {
//            o1 = storage.getFaveUsersOnline(offset, true);
//            o2 = storage.getInfoFromStorageOnline(true);
//        } else {
//            o1 = storage.getFaveUsersOffline().concatWith(storage.getFaveUsersOnline(offset, false));
//            o2 = storage.getInfoFromStorageOffline().concatWith(storage.getInfoFromStorageOnline(false));
//        }
//        return Observable.combineLatest(
//                o1,
//                o2,
//                (vkApiUserFulls, storageInfo) -> {
//                    info = storageInfo;
//                    ignoredIds.clear();
//                    ignoredIds.addAll(info.ignoredMap.get(StorageInfo.USERS_IGNORED_KEY));
//                    ignoredIds.addAll(newIgnoredIds);
//                    for (Iterator<VKApiUserFull> iterator = vkApiUserFulls.iterator(); iterator.hasNext(); ) {
//                        VKApiUserFull vkApiUser = iterator.next();
//                        if (ignoredIds.contains(vkApiUser.id)) {
//                            iterator.remove();
//                            continue;
//                        }
//                        if (deletedIds.contains(vkApiUser.id)) {
//                            iterator.remove();
//                        }
//                    }
//                    users.clear();
//                    users.addAll(vkApiUserFulls);
//                    return users;
//                }
//        ).subscribeOn(Schedulers.io());
//    }
}
