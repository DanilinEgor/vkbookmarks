package xyz.egor_d.vkbookmarks.presentation.users;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiUserFull;

import butterknife.Bind;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;

public class SimpleVkUsersAdapter extends SimpleVkDataAdapter<VKApiUserFull> {
    public static class UserViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;
        @Bind(R.id.container)
        View container;
        @Bind(R.id.tags_container)
        LinearLayout tagsContainer;
        @Bind(R.id.add_tag)
        View addTag;

        public UserViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public View getAddTagView(final RecyclerView.ViewHolder holder) {
        return ((UserViewHolder) holder).addTag;
    }

    @Override
    public RecyclerView.ViewHolder getVH(final ViewGroup parent, final int viewType) {
        return new UserViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_user, parent, false));
    }

    @Override
    public void bindVH(final RecyclerView.ViewHolder vh, final int position) {
        UserViewHolder holder = (UserViewHolder) vh;
        VKApiUserFull user = data.get(position);

        holder.name.setText(String.format("%s %s", user.first_name, user.last_name));
        Picasso.with(holder.itemView.getContext()).load(user.photo_100).transform(new CropCircleTransformation()).into(holder.avatar);
        for (int i = holder.tagsContainer.getChildCount() - 1; i >= 0; i--) {
            holder.tagsContainer.removeViewAt(i);
        }

        fillTags(vh.itemView.getContext(), holder.tagsContainer, user);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(holder.getAdapterPosition());
            }
        });

        holder.container.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onItemClicked(holder.getAdapterPosition());
            }
        });
        holder.tagsContainer.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
        holder.addTag.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
    }
}
