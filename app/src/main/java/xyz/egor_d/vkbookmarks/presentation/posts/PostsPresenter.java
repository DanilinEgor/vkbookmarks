package xyz.egor_d.vkbookmarks.presentation.posts;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiPost;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class PostsPresenter extends BasePresenter<IPostsView> {
    private final IStorage storage;
    private final CompositeSubscription subscription = new CompositeSubscription();
    private List<VKApiPost> posts = new ArrayList<>();
    private int lastPos;
    private VKApiPost lastPost;
    private Subscription removeLinkFromFaveSubscription;

    @Inject
    public PostsPresenter(IStorage storage) {
        this.storage = storage;
    }

    @Override
    public void onStart() {
        refresh(false);
    }

    @Override
    public void onStop() {
        storage.setFavePostsOffline(posts);
        subscription.clear();
    }

    @Override
    public void itemClicked(final int position) {

    }

    public void refresh(boolean force) {
        Observable<List<VKApiPost>> o1;
        if (force) {
            o1 = storage.getFavePostsOnline(0, true);
        } else {
            o1 = storage.getFavePostsOffline().concatWith(storage.getFavePostsOnline(0, false));
        }
        subscription.add(o1
                .map(vkPosts -> {
                    posts.clear();
                    posts.addAll(vkPosts);
                    return posts;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        vkPosts -> getView().showPosts(vkPosts),
                        throwable -> {
                            Timber.e("Error:", throwable);
                            getView().showError(throwable.getMessage());
                            getView().setRefreshing(false);
                        },
                        () -> getView().setRefreshing(false)
                ));
    }

    public void itemDeleteClicked(int pos) {
        lastPos = pos;
        lastPost = posts.get(pos);

        posts.remove(lastPost);

        removeLinkFromFaveSubscription =
                storage
                        .removeLike("post", lastPost.from_id, lastPost.id)
                        .delaySubscription(2, TimeUnit.SECONDS)
                        .subscribe(
                                success -> {
                                },
                                throwable -> {
                                }
                        );

        subscription.add(removeLinkFromFaveSubscription);

        getView().notifyPositionRemoved(pos);
        getView().showSnackbar(lastPost, true);
    }

    public void undoPressed() {
        if (removeLinkFromFaveSubscription != null) {
            removeLinkFromFaveSubscription.unsubscribe();
        }
        posts.add(lastPos, lastPost);
        getView().notifyPositionInserted(lastPos);
    }

    @Override
    public void requestUpdate(final int offset) {

    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }
}
