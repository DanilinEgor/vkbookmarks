package xyz.egor_d.vkbookmarks.presentation.main;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.subscriptions.CompositeSubscription;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.domain.Preferences;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;
import xyz.egor_d.vkbookmarks.presentation.links.SimpleVkLinksFragment;
import xyz.egor_d.vkbookmarks.presentation.photos.SimpleVkPhotosFragment;
import xyz.egor_d.vkbookmarks.presentation.posts.SimpleVkPostsFragment;
import xyz.egor_d.vkbookmarks.presentation.users.SimpleVkUsersFragment;
import xyz.egor_d.vkbookmarks.presentation.videos.SimpleVkVideosFragment;

public class MainPresenter extends BasePresenter<IMainView> {
    private final Preferences preferences;
    IStorage storage;
    CompositeSubscription subscription = new CompositeSubscription();

    @Inject
    public MainPresenter(IStorage storage, Preferences preferences) {
        this.storage = storage;
        this.preferences = preferences;
    }

    @Override
    public void onStart() {
//        if (!preferences.getBoolean(Preferences.STORAGE_SYNCED, true)) {
//            subscription.add(
//                    storage.getInfoFromStorageOffline()
//                            .flatMap(storageInfo -> storage.saveInfoToStorageOnline(storageInfo))
//                            .subscribe(
//                                    success -> {
//                                        preferences.putBoolean(Preferences.STORAGE_SYNCED, success);
//                                    },
//                                    throwable -> {
//                                    }
//                            )
//            );
//        }
//        if (!preferences.getBoolean(Preferences.DELETED_SYNCED, true)) {
//            Set<Integer> ids = new Gson().fromJson(preferences.getString(Preferences.DELETED_IDS_KEY, ""), new TypeToken<TreeSet<Integer>>() {
//            }.getType());
//            Set<Integer> unsuccessfulIds = new TreeSet<>();
//            subscription.add(
//                    Observable.from(ids)
//                            .flatMap(id ->
//                                    Observable.combineLatest(
//                                            Observable.just(id),
//                                            storage.removeUserFromFave(id),
//                                            Pair::create
//                                    ))
//                            .subscribe(
//                                    pair -> {
//                                        if (!pair.second) {
//                                            unsuccessfulIds.add(pair.first);
//                                        }
//                                    },
//                                    throwable -> {
//                                    },
//                                    () -> {
//                                        preferences.putString(Preferences.DELETED_IDS_KEY, new Gson().toJson(unsuccessfulIds));
//                                        preferences.putBoolean(Preferences.DELETED_SYNCED, unsuccessfulIds.isEmpty());
//                                    }
//                            ));
//        }
    }

    private void add(final int i) {
        if (i > 100) return;
        subscription.add(storage.addFave(i).delay(500, TimeUnit.MILLISECONDS).subscribe(aBoolean -> add(i + 1)));
    }

    @Override
    public void onStop() {
        subscription.clear();
    }

    @Override
    public void itemClicked(final int position) {

    }

    @Override
    public void itemDeleteClicked(final int position) {

    }

    @Override
    public void undoPressed() {

    }

    @Override
    public void requestUpdate(final int offset) {

    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }

    public void clearStorageInfo() {
//        subscription.add(
//                storage
//                        .saveInfoToStorageOnline(new StorageInfo())
//                        .subscribe()
//        );
    }

    public void onClicked(MainAdapter.MainMenuEnum type) {
        switch (type) {
            case PHOTOS:
                getView().setFragment(new SimpleVkPhotosFragment());
                break;
            case POSTS:
                getView().setFragment(new SimpleVkPostsFragment());
                break;
            case VIDEOS:
                getView().setFragment(new SimpleVkVideosFragment());
                break;
            case USERS:
                getView().setFragment(new SimpleVkUsersFragment());
                break;
            case LINKS:
                getView().setFragment(new SimpleVkLinksFragment());
                break;
        }
    }
}
