package xyz.egor_d.vkbookmarks.presentation.posts;

import android.support.design.widget.Snackbar;

import com.vk.sdk.api.model.VKApiPost;

import javax.inject.Inject;

import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;
import xyz.egor_d.vkbookmarks.presentation.VkDataFragment;

public class SimpleVkPostsFragment extends VkDataFragment<VKApiPost> {
    @Inject
    SimpleVkPostsPresenter presenter;

    @Override
    public SimpleVkDataAdapter<VKApiPost> getAdapter() {
        return new SimpleVkPostsAdapter();
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSnackbar(final VKApiPost post, final boolean removed) {
        Snackbar
                .make(
                        mRecyclerView,
                        R.string.post_deleted_from_fave,
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }
}
