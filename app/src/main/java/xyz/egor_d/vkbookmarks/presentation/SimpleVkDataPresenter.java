package xyz.egor_d.vkbookmarks.presentation;

import com.vk.sdk.api.model.Identifiable;
import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.TagsInfo;

public abstract class SimpleVkDataPresenter<T extends VKApiModel & Identifiable> extends BasePresenter<IBaseView<T>> {
    protected IStorage storage;
    private final CompositeSubscription subscription = new CompositeSubscription();
    protected final List<T> data = new ArrayList<>();
    protected int lastPos;
    protected T lastItem;
    private Subscription removeFromFaveSubscription;
    private List<Tag> filterTags = new ArrayList<>();
    private Func1<List<T>, List<T>> filterTagsTransformer = ts -> {
        List<T> res = new ArrayList<>();
        if (!filterTags.isEmpty()) {
            for (T item : ts) {
                List<Tag> itemTags = item.appTags;
                if (itemTags != null) {
                    for (Tag itemTag : itemTags) {
                        if (filterTags.contains(itemTag)) {
                            res.add(item);
                            break;
                        }
                    }
                }
            }
        } else {
            res.addAll(ts);
        }
        return res;
    };

    private Observable.Transformer<List<T>, List<T>> tagsTransformer =
            listObservable ->
                    listObservable
                            .map(items -> Observable.combineLatest(
                                    Observable.just(items),
                                    storage.getTagsInfoOffline(),
                                    (resItems, tagsInfo) -> {
                                        Map<Integer, List<Tag>> map = getTagsMap(tagsInfo);
                                        for (T item : resItems) {
                                            item.appTags = map.get(item.getId());
                                        }
                                        return resItems;
                                    })
                                    .toBlocking()
                                    .first()
                            );


    public SimpleVkDataPresenter(final IStorage storage) {
        this.storage = storage;
    }

    protected abstract Map<Integer, List<Tag>> getTagsMap(final TagsInfo tagsInfo);

    public abstract Observable<List<T>> getFaveOffline();

    public abstract Observable<List<T>> getFaveOnline(int offset, boolean force);

    protected abstract Observable<Boolean> removeFromFave();

    public abstract void itemClicked(final int position);

    @Override
    public ArrayList<Tag> getFilterTags() {
        return (ArrayList<Tag>) filterTags;
    }

    public void setFilterTags(final ArrayList<Tag> filterTags) {
        if (this.filterTags.equals(filterTags)) return;
        this.filterTags.clear();
        Set<Tag> set = new TreeSet<>();
        set.addAll(filterTags);
        this.filterTags.addAll(set);
        getView().setRefreshing(true);
        subscription.add(
                getFaveOffline()
                        .observeOn(AndroidSchedulers.mainThread())
                        .compose(tagsTransformer)
                        .map(filterTagsTransformer)
                        .flatMap(userFulls -> {
                            synchronized (data) {
                                data.clear();
                                data.addAll(userFulls);
                            }
                            return Observable.just(data);
                        })
                        .subscribe(
                                userFulls -> {
                                    getView().showData(userFulls);
                                },
                                throwable -> {
                                    Timber.e("Error:", throwable);
                                    getView().showError(throwable.getMessage());
                                    getView().setRefreshing(false);
                                },
                                () -> getView().setRefreshing(false)
                        )
        );
    }

    @Override
    public void onStart() {
        getView().setRefreshing(true);
        refresh();
    }

    private void refresh() {
        subscription.add(
                getFaveOffline()
                        .concatWith(
                                Observable.zip(
                                        getFaveOnline(0, false),
                                        storage.getTagsInfoOnline(false),
                                        (ts, tagsInfo) -> ts
                                )
                        )
                        .subscribeOn(Schedulers.io())
                        .compose(tagsTransformer)
                        .map(filterTagsTransformer)
                        .flatMap(userFulls -> {
                            data.clear();
                            data.addAll(userFulls);
                            return Observable.just(data);
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                userFulls -> {
                                    getView().showData(userFulls);
                                },
                                throwable -> {
                                    Timber.e("Error:", throwable);
                                    getView().showError(throwable.getMessage());
                                    getView().setRefreshing(false);
                                },
                                () -> getView().setRefreshing(false)
                        )
        );
    }

    @Override
    public void onStop() {
        subscription.clear();
    }


    public void requestUpdate(int offset) {
        getView().setRefreshing(true);
        Observable<List<T>> o;
        if (offset == 0) {
            o = Observable.zip(
                    getFaveOnline(offset, true),
                    storage.getTagsInfoOnline(true),
                    (ts, tagsInfo) -> ts
            );
        } else {
            o = getFaveOnline(offset, true);
        }
        subscription.add(
                //TODO: запросит юзеров начиная с offset, переделать потом на подписку на storage и запрос обновления
                o.observeOn(AndroidSchedulers.mainThread())
                        .compose(tagsTransformer)
                        .map(filterTagsTransformer)
                        .flatMap(userFulls -> {
                            synchronized (data) {
                                data.clear();
                                data.addAll(userFulls);
                            }
                            return Observable.just(data);
                        })
                        .subscribe(
                                userFulls -> {
                                    getView().showData(userFulls);
                                },
                                throwable -> {
                                    Timber.e("Error:", throwable);
                                    getView().showError(throwable.getMessage());
                                    getView().setRefreshing(false);
                                },
                                () -> getView().setRefreshing(false)
                        )
        );
    }

    public void itemSwipedLeft(int pos) {
        lastPos = pos;
        lastItem = data.get(pos);

        data.remove(lastItem);

        getView().removeItem(pos);
        getView().showSnackbar(lastItem, false);
    }

    @Override
    public void itemDeleteClicked(final int pos) {
        lastPos = pos;
        lastItem = data.get(pos);

        data.remove(lastItem);

        removeFromFaveSubscription =
                //TODO: проверить, что все возвращают нужный id или вызывать unlike
                removeFromFave()
                        .delaySubscription(2, TimeUnit.SECONDS)
                        .subscribe(
                                success -> {
                                },
                                throwable -> {
                                }
                        );

        getView().removeItem(pos);
        getView().showSnackbar(lastItem, true);
    }

    @Override
    public void addTagClicked(final int position) {
        lastPos = position;
        lastItem = data.get(lastPos);
        getView().chooseTag((ArrayList<Tag>) lastItem.appTags, position);
    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {
        if (changed || (lastItem.appTags == null && !tags.isEmpty()) || (lastItem.appTags != null && !tags.equals(lastItem.appTags))) {
            storage.getTagsInfoOffline()
                    .map(tagsInfo -> {
                        lastItem.appTags = tags;
                        getTagsMap(tagsInfo).put(lastItem.getId(), tags);
                        getView().changeItem(lastPos, lastItem);
                        storage.saveTagsInfoOffline(tagsInfo);
                        filterTags.retainAll(tagsInfo.allTags);
                        refresh();
                        return tagsInfo;
                    })
                    .flatMap(tagsInfo -> storage.saveTagsInfoOnline(tagsInfo))
                    .subscribe(success -> {
                    }, throwable -> {
                    });
        }
    }

    public void undoPressed() {
        if (removeFromFaveSubscription != null) {
            removeFromFaveSubscription.unsubscribe();
        }
        data.add(lastPos, lastItem);
        getView().addItem(lastPos, lastItem);
    }
}
