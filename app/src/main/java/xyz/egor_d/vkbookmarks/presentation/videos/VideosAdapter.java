package xyz.egor_d.vkbookmarks.presentation.videos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiVideo;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.VideoViewHolder> {
    List<VKApiVideo> videos = new ArrayList<>();
    EventListener eventListener;

    public static class VideoViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.name)
        TextView name;
        @Bind(R.id.avatar)
        ImageView avatar;
        @Bind(R.id.delete)
        ImageView delete;

        public VideoViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface EventListener {
        void onDeleteClicked(int pos);
    }

    public void setEventListener(final EventListener eventListener) {
        this.eventListener = eventListener;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        return new VideoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false));
    }

    @Override
    public void onBindViewHolder(final VideoViewHolder holder, final int position) {
        VKApiVideo video = videos.get(position);

        holder.name.setText(String.format("%s", video.title));
        Picasso.with(holder.itemView.getContext()).load(video.photo_130).into(holder.avatar);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return videos.size();
    }

    public void setVideos(final List<VKApiVideo> videos) {
        this.videos = videos;
        notifyDataSetChanged();
    }
}
