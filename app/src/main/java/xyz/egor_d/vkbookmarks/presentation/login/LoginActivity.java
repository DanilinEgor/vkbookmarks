package xyz.egor_d.vkbookmarks.presentation.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.IntroActivity;
import xyz.egor_d.vkbookmarks.presentation.MainActivity;

public class LoginActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences getPrefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean isFirstStart = getPrefs.getBoolean("firstStart", true);
        if (isFirstStart) {
            Intent i = new Intent(LoginActivity.this, IntroActivity.class);
            startActivity(i);
            getPrefs.edit().putBoolean("firstStart", false).apply();
        } else if (VKSdk.isLoggedIn()) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }
        setContentView(R.layout.fragment_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login_button)
    public void loginButtonClicked() {
        VKSdk.login(this, "friends", "wall");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putString("token", res.accessToken).apply();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finish();
            }

            @Override
            public void onError(VKError error) {
                switch (error.errorCode) {
                    case VKError.VK_API_ERROR:
                        Toast.makeText(LoginActivity.this, error.apiError.errorMessage, Toast.LENGTH_SHORT).show();
                        break;
                    default:
                        Toast.makeText(LoginActivity.this, error.errorMessage, Toast.LENGTH_SHORT).show();
                        break;
                }

            }
        })) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
