package xyz.egor_d.vkbookmarks.presentation;

import android.content.Intent;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;
import java.util.List;

public interface IBaseView<T> {
    void removeItem(int pos);

    void addItem(int pos, final T lastItem);

    void changeItem(int lastPos, final T item);

    void showError(String message);

    void showData(List<T> data);

    void setRefreshing(boolean refreshing);

    void sendIntent(final Intent intent);

    // removed == true if remove from fave, false if moved to ignore
    void showSnackbar(T lastItem, boolean removed);

    void chooseTag(ArrayList<Tag> tags, final int position);

}
