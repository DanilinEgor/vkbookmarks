package xyz.egor_d.vkbookmarks.presentation;

import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.decoration.ItemShadowDecorator;
import com.h6ah4i.android.widget.advrecyclerview.decoration.SimpleListDividerDecorator;
import com.vk.sdk.api.model.Identifiable;
import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.android.schedulers.AndroidSchedulers;
import xyz.egor_d.vkbookmarks.FabDialogMorphSetup;
import xyz.egor_d.vkbookmarks.PaginationTool;
import xyz.egor_d.vkbookmarks.R;

public abstract class VkDataFragment<T extends VKApiModel & Identifiable> extends BaseFragment implements IBaseView<T>, SwipeRefreshLayout.OnRefreshListener {
    private static final int RC_FILTER_DIALOG_ACTIVITY = 1;
    private static final int RC_TAG_DIALOG_ACTIVITY = 2;
    private int page = 0;

    @Bind(R.id.users_swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.users_recycler_view)
    protected RecyclerView mRecyclerView;
    @Bind(R.id.empty_data_container)
    View emptyDataContainer;
    @Bind(R.id.fab)
    FloatingActionButton fab;

    private SimpleVkDataAdapter<T> mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public abstract SimpleVkDataAdapter<T> getAdapter();

    protected abstract void inject();

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_vk_data, container, false);
        ButterKnife.bind(this, view);

        mAdapter = getAdapter();
//        mAdapter.setHasStableIds(true);
        mAdapter.setEventListener(new SimpleVkDataAdapter.EventListener() {
            @Override
            public void onDeleteClicked(final int pos) {
                getPresenter().itemDeleteClicked(pos);
            }

            @Override
            public void onItemClicked(final int position) {
                getPresenter().itemClicked(position);
            }

            @Override
            public void onAddTagClicked(final int position) {
                getPresenter().addTagClicked(position);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        layoutManager = getLayoutManager();
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
//        mRecyclerView.setItemAnimator(new SlideInRightAnimator());

        PaginationTool
                .buildPagingObservable(mRecyclerView)
                .getPagingObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(offset -> {
                    getPresenter().requestUpdate(calculateOffset());
                }, throwable -> {
                    showError(throwable.getMessage());
                });

        if (!supportsViewElevation()) {
            mRecyclerView.addItemDecoration(new ItemShadowDecorator((NinePatchDrawable) ContextCompat.getDrawable(getContext(), R.drawable.material_shadow_z1)));
        }
        mRecyclerView.addItemDecoration(new SimpleListDividerDecorator(ContextCompat.getDrawable(getContext(), R.drawable.list_divider_h), true));

        return view;
    }

    @OnClick(R.id.fab)
    public void fabClicked() {
        Intent intent = new Intent(getActivity(), FilterDialogActivity.class);
        intent.putParcelableArrayListExtra(FilterDialogActivity.TAGS_LIST, getPresenter().getFilterTags());
        intent.putExtra(FabDialogMorphSetup.EXTRA_SHARED_ELEMENT_START_COLOR,
                ContextCompat.getColor(getActivity(), R.color.colorAccent));
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(), fab,
                    getString(R.string.transition_filter_dialog_activity));
            startActivityForResult(intent, RC_FILTER_DIALOG_ACTIVITY, options.toBundle());
        } else {
            startActivityForResult(intent, RC_FILTER_DIALOG_ACTIVITY);
        }
    }

    protected int calculateOffset() {
        return (++page) * PaginationTool.DEFAULT_LIMIT;
    }


    protected RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    private boolean supportsViewElevation() {
        return (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
    }

    @Override
    public void onRefresh() {
        page = 0;
        getPresenter().requestUpdate(0);
    }

    @Override
    public void removeItem(final int pos) {
        mAdapter.removeItem(pos);
        checkItemsCount();
    }

    @Override
    public void addItem(final int pos, final T item) {
        mAdapter.addItem(pos, item);
        checkItemsCount();
    }

    @Override
    public void changeItem(final int lastPos, final T item) {
        mAdapter.changeItem(lastPos, item);
        checkItemsCount();
    }

    @Override
    public void showError(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        checkItemsCount();
    }

    @Override
    public void showData(final List<T> items) {
        fab.show();
        mAdapter.setData(items);
        checkItemsCount();
    }

    private void checkItemsCount() {
        if (mAdapter.getItemCount() == 0) {
            emptyDataContainer.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            emptyDataContainer.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setRefreshing(final boolean refreshing) {
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(refreshing));
    }

    @Override
    public void chooseTag(ArrayList<Tag> tags, final int position) {
        Intent intent = new Intent(getActivity(), TagDialogActivity.class);
        intent.putParcelableArrayListExtra(TagDialogActivity.TAGS_LIST, tags == null ? new ArrayList<Tag>() : tags);
        intent.putExtra(FabDialogMorphSetup.EXTRA_SHARED_ELEMENT_START_COLOR,
                getStartAnimColor());
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            View targetView = mAdapter.getAddTagView(mRecyclerView.getChildViewHolder(layoutManager.findViewByPosition(position)));
            if (targetView != null) {
                ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(getActivity(),
                        targetView,
                        getString(R.string.transition_tag_dialog_activity));
                startActivityForResult(intent, RC_TAG_DIALOG_ACTIVITY, options.toBundle());
            } else {
                startActivityForResult(intent, RC_TAG_DIALOG_ACTIVITY);
            }
        } else {
            startActivityForResult(intent, RC_TAG_DIALOG_ACTIVITY);
        }
    }

    protected int getStartAnimColor() {
        return Color.parseColor("#60000000");
    }

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RC_TAG_DIALOG_ACTIVITY:
                ArrayList<Tag> tags = data.getParcelableArrayListExtra(TagDialogActivity.TAGS_LIST);
                boolean changed = data.getBooleanExtra(TagDialogActivity.CHANGED, false);
                getPresenter().tagsReturned(tags, changed);
                break;
            case RC_FILTER_DIALOG_ACTIVITY:
                ArrayList<Tag> resTags = data.getParcelableArrayListExtra(FilterDialogActivity.TAGS_LIST);
                getPresenter().setFilterTags(resTags);
                break;
        }
    }

    @Override
    public void sendIntent(final Intent intent) {
        sendIntentFromFragment(intent);
    }
}
