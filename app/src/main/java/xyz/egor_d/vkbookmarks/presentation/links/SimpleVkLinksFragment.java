package xyz.egor_d.vkbookmarks.presentation.links;

import android.support.design.widget.Snackbar;

import com.vk.sdk.api.model.VKApiLink;

import javax.inject.Inject;

import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;
import xyz.egor_d.vkbookmarks.presentation.VkDataFragment;

public class SimpleVkLinksFragment extends VkDataFragment<VKApiLink> {
    @Inject
    SimpleVkLinksPresenter presenter;

    @Override
    public SimpleVkDataAdapter<VKApiLink> getAdapter() {
        return new SimpleVkLinksAdapter();
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSnackbar(final VKApiLink link, final boolean removed) {
        Snackbar
                .make(
                        mRecyclerView,
                        String.format("%s %s", link.title, getString(R.string.deleted_link)),
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }
}
