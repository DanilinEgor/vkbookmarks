package xyz.egor_d.vkbookmarks.presentation.videos;

import android.support.design.widget.Snackbar;

import com.vk.sdk.api.model.VKApiVideo;

import javax.inject.Inject;

import xyz.egor_d.vkbookmarks.App;
import xyz.egor_d.vkbookmarks.presentation.MainActivity;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;
import xyz.egor_d.vkbookmarks.presentation.VkDataFragment;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class SimpleVkVideosFragment extends VkDataFragment<VKApiVideo> {
    @Inject
    SimpleVkVideosPresenter presenter;

    @Override
    public SimpleVkDataAdapter<VKApiVideo> getAdapter() {
        return new SimpleVkVideosAdapter();
    }

    @Override
    protected void inject() {
        ((App) getActivity().getApplicationContext()).getMainComponent().inject(this);
    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSnackbar(final VKApiVideo video, final boolean removed) {
        Snackbar
                .make(
                        mRecyclerView,
                        String.format("%s %s", video.title, getString(R.string.deleted_from_fave_n)),
                        Snackbar.LENGTH_SHORT
                )
                .setAction(
                        R.string.undo,
                        v -> {
                            getPresenter().undoPressed();
                        }
                )
                .show();
    }
}
