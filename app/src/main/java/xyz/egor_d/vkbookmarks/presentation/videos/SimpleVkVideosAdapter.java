package xyz.egor_d.vkbookmarks.presentation.videos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiVideo;

import butterknife.Bind;
import butterknife.ButterKnife;
import xyz.egor_d.vkbookmarks.R;
import xyz.egor_d.vkbookmarks.presentation.SimpleVkDataAdapter;

public class SimpleVkVideosAdapter extends SimpleVkDataAdapter<VKApiVideo> {
    public static class VideoViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.likes_count)
        TextView likesCount;
        @Bind(R.id.views)
        TextView views;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.delete)
        View delete;
        @Bind(R.id.container)
        View container;
        @Bind(R.id.tags_container)
        ViewGroup tagsContainer;

        public VideoViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public View getAddTagView(final RecyclerView.ViewHolder holder) {
        return ((VideoViewHolder) holder).tagsContainer.getChildAt(0);
    }

    @Override
    public RecyclerView.ViewHolder getVH(final ViewGroup parent, final int viewType) {
        return new VideoViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false));
    }

    @Override
    public void bindVH(final RecyclerView.ViewHolder vh, final int position) {
        VideoViewHolder holder = (VideoViewHolder) vh;
        VKApiVideo video = data.get(position);

        holder.title.setText(String.format("%s", video.title));
        holder.views.setText(String.format("%s %s", video.views, holder.views.getContext().getString(R.string.views)));
        holder.likesCount.setText(String.format("%s", data.get(position).likes));

        for (int i = holder.tagsContainer.getChildCount() - 1; i >= 1; i--) {
            holder.tagsContainer.removeViewAt(i);
        }

        fillTags(vh.itemView.getContext(), holder.tagsContainer, video);

        Picasso.with(holder.itemView.getContext()).load(video.photo_320).into(holder.image);
        holder.delete.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onDeleteClicked(holder.getAdapterPosition());
            }
        });
        holder.container.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onItemClicked(holder.getAdapterPosition());
            }
        });
        holder.tagsContainer.setOnClickListener(v -> {
            if (eventListener != null) {
                eventListener.onAddTagClicked(holder.getAdapterPosition());
            }
        });
    }
}
