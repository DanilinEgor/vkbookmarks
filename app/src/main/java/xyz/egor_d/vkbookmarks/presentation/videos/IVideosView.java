package xyz.egor_d.vkbookmarks.presentation.videos;

import com.vk.sdk.api.model.VKApiVideo;

import java.util.List;

public interface IVideosView {
    void showError(String message);

    void showVideos(List<VKApiVideo> videos);

    void setRefreshing(boolean refreshing);

    void showSnackbar(VKApiVideo video, boolean removed);

    void notifyPositionRemoved(int pos);

    void notifyPositionInserted(int pos);
}
