package xyz.egor_d.vkbookmarks.presentation.photos;

import com.vk.sdk.api.model.VKApiPhoto;

import java.util.List;

public interface IPhotosView {
    void showError(String message);

    void showPhotos(List<VKApiPhoto> photos);

    void setRefreshing(boolean refreshing);

    void showSnackbar(VKApiPhoto photo, boolean removed);

    void notifyPositionRemoved(int pos);

    void notifyPositionInserted(int pos);
}
