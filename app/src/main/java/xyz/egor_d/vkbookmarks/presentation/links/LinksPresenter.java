package xyz.egor_d.vkbookmarks.presentation.links;

import android.content.Intent;
import android.net.Uri;

import com.vk.sdk.api.model.Tag;
import com.vk.sdk.api.model.VKApiLink;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.presentation.BasePresenter;

public class LinksPresenter extends BasePresenter<ILinksView> {
    private final IStorage storage;
    private final CompositeSubscription subscription = new CompositeSubscription();
    private List<VKApiLink> links = new ArrayList<>();
    private int lastPos;
    private VKApiLink lastLink;
    private Subscription removeLinkFromFaveSubscription;

    @Inject
    public LinksPresenter(IStorage storage) {
        this.storage = storage;
    }

    @Override
    public void onStart() {
        refresh(false);
    }

    @Override
    public void onStop() {
        storage.setFaveLinksOffline(links);
        subscription.clear();
    }

    public void refresh(boolean force) {
        Observable<List<VKApiLink>> o1;
        if (force) {
            o1 = storage.getFaveLinksOnline(0, true);
        } else {
            o1 = storage.getFaveLinksOffline().concatWith(storage.getFaveLinksOnline(0, false));
        }
        subscription.add(o1
                .map(vkApiLinks -> {
                    links.clear();
                    links.addAll(vkApiLinks);
                    return links;
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        vkApiLinks -> getView().showLinks(vkApiLinks),
                        throwable -> {
                            Timber.e("Error:", throwable);
                            getView().showError(throwable.getMessage());
                            getView().setRefreshing(false);
                        },
                        () -> getView().setRefreshing(false)
                ));
    }

    public void itemDeleteClicked(int pos) {
        lastPos = pos;
        lastLink = links.get(pos);

        links.remove(lastLink);

        removeLinkFromFaveSubscription =
                storage
                        .removeLinkFromFave(lastLink.id)
                        .delaySubscription(2, TimeUnit.SECONDS)
                        .subscribe(
                                success -> {
                                },
                                throwable -> {
                                }
                        );

        subscription.add(removeLinkFromFaveSubscription);

        getView().notifyPositionRemoved(pos);
        getView().showSnackbar(lastLink, true);
    }

    public void undoPressed() {
        if (removeLinkFromFaveSubscription != null) {
            removeLinkFromFaveSubscription.unsubscribe();
        }
        links.add(lastPos, lastLink);
        getView().notifyPositionInserted(lastPos);
    }

    @Override
    public void requestUpdate(final int offset) {

    }

    @Override
    public void addTagClicked(final int position) {

    }

    @Override
    public void tagsReturned(final ArrayList<Tag> tags, boolean changed) {

    }

    public void itemClicked(final int position) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(links.get(position).url));
        getView().sendIntent(intent);
    }
}
