package xyz.egor_d.vkbookmarks.data;

import android.text.TextUtils;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiCommunity;
import com.vk.sdk.api.model.VKApiCommunityArray;
import com.vk.sdk.api.model.VKApiCommunityFull;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiOwner;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKApiPost;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKApiVideo;
import com.vk.sdk.api.model.VKLinkArray;
import com.vk.sdk.api.model.VKList;
import com.vk.sdk.api.model.VKPhotoArray;
import com.vk.sdk.api.model.VKPostArray;
import com.vk.sdk.api.model.VKUsersArray;
import com.vk.sdk.api.model.VKVideoArray;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.functions.Func1;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.JSONUtils;
import xyz.egor_d.vkbookmarks.PaginationTool;
import xyz.egor_d.vkbookmarks.VkObservable;
import xyz.egor_d.vkbookmarks.domain.Preferences;

public class Storage implements IStorage {
    //    public static final String IGNORED_KEY = "ignored";
    private final String DEFAULT_EMPTY_LIST = "{response:{count:0,items:[]}}";
    public static final String LINKS_URL = "fave.getLinks";
    public static final String USERS_URL = "fave.getUsers";
    public static final String POSTS_URL = "fave.getPosts";
    public static final String PHOTOS_URL = "fave.getPhotos";
    public static final String VIDEOS_URL = "fave.getVideos";
    private static final String LAST_INFO_UPDATE_KEY = "last_update_info";
    public static final String LAST_USERS_UPDATE_KEY = "last_update_users";
    public static final String LAST_LINKS_UPDATE_KEY = "last_update_links";
    public static final String LAST_PHOTOS_UPDATE_KEY = "last_update_photos";
    public static final String LAST_POSTS_UPDATE_KEY = "last_update_posts";
    public static final String LAST_VIDEOS_UPDATE_KEY = "last_update_videos";
    public static final String LAST_TAGS_UPDATE_KEY = "last_update_tags";
    public static final String COMMUNITIES = "communities";
    public static final String USERS = "users";
    public static final String TAGS_INFO = "tags_info";
    public static final String TAGS = "tags";
    public static final String POST_TAGS = "post_tags";
    public static final String PHOTO_TAGS = "photo_tags";
    public static final String VIDEO_TAGS = "video_tags";
    public static final String LINK_TAGS = "link_tags";
    public static final String USER_TAGS = "user_tags";
    private final Preferences preferences;
    private final Gson gson;

    public Storage(Preferences preferences, Gson gson) {
        this.preferences = preferences;
        this.gson = gson;
    }

    /*
    @RxLogObservable
    @Override
    public Observable<Boolean> saveInfoToStorageOnline(StorageInfo info) {
        info.lastSyncTime = System.currentTimeMillis();
        preferences.putString(IGNORED_KEY, gson.toJson(info));
        return VkObservable.create(new VKRequest("storage.set").addExtraParameter("key", IGNORED_KEY).addExtraParameter("value", gson.toJson(info)).addExtraParameter("global", 0))
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, VkStorageResponse.class))
                .map(vkStorageResponse -> vkStorageResponse.response == 1)
                .flatMap(success -> {
                    if (success) {
                        return Observable.empty();
                    } else {
                        return Observable.error(new Throwable());
                    }
                });
    }

    @RxLogObservable
    @Override
    public Observable<StorageInfo> getInfoFromStorageOnline(boolean force) {
        long lastUpdateTime = preferences.getLong(LAST_INFO_UPDATE_KEY, 0);
        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
            return VkObservable.create(new VKRequest("storage.get").addExtraParameter("key", IGNORED_KEY))
                    .map(vkResponse -> {
                        preferences.putLong(LAST_INFO_UPDATE_KEY, System.currentTimeMillis());
                        return gson.fromJson(vkResponse.responseString, VkStorageGetResponse.class).response;
                    })
                    .map(json -> {
                        StorageInfo info;
                        if (json.isEmpty()) {
                            info = new StorageInfo();
                        } else {
                            info = gson.fromJson(json, StorageInfo.class);
                        }
                        preferences.putString(IGNORED_KEY, json);
                        return info;
                    });
        } else {
            return Observable.empty();
        }
    }

    @RxLogObservable
    @Override
    public Observable<StorageInfo> getInfoFromStorageOffline() {
        String s = preferences.getString(IGNORED_KEY, "");
        if (s.isEmpty()) {
            return Observable.empty();
        } else {
            return Observable.just(gson.fromJson(s, StorageInfo.class));
        }
    }

    @RxLogObservable
    @Override
    public Observable<List<String>> getKeysFromStorage() {
        return VkObservable.create(new VKRequest("storage.getKeys"))
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, VkStorageGetKeysResponse.class).response);
    }
    */

    /**
     * GET FAVE DATA FROM VK ONLINE
     *
     * @param type   - one of Storage.Type enum
     * @param offset - offset for VkRequest
     * @param force  - if force, then do request anyway, else check only for last update time
     * @param <T>    - type, that Vk will return
     * @return rx.Observable with List of T
     */

    @RxLogObservable
    private <T> Observable<List<T>> getOnline(final Type type, final int offset, final boolean force) {
        long lastUpdateTime = preferences.getLong(type.getLastUpdateKey(), 0);
        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
            return VkObservable
                    .create(
                            new VKRequest(type.getRequestUrl())
                                    .addExtraParameter(VKApiConst.OFFSET, offset)
                                    .addExtraParameter(VKApiConst.COUNT, PaginationTool.DEFAULT_LIMIT)
                    )
                    .map((Func1<VKResponse, List<T>>) vkResponse -> {
                        try {
                            preferences.putLong(type.getLastUpdateKey(), System.currentTimeMillis());
                            List<T> initList = null;
                            if (type != Type.USERS) {
                                initList = (List<T>) Storage.this.getFaveOffline(type)
                                        .toBlocking()
                                        .first();
                            } else {
                                initList = (List<T>) Storage.this.getFaveUsersOffline()
                                        .toBlocking()
                                        .first();
                            }
                            List<T> list = new ArrayList<>();
                            switch (type) {
                                case POSTS:
                                    list = (List<T>) new VKPostArray().parse(vkResponse.json).items;
                                    break;
                                case USERS:
                                    list = (List<T>) new VKUsersArray().parse(vkResponse.json).items;
                                    break;
                                case VIDEOS:
                                    list = (List<T>) new VKVideoArray().parse(vkResponse.json).items;
                                    break;
                                case PHOTOS:
                                    list = (List<T>) new VKPhotoArray().parse(vkResponse.json).items;
                                    break;
                                case LINKS:
                                    list = (List<T>) new VKLinkArray().parse(vkResponse.json).items;
                                    break;
                            }
                            if (offset <= initList.size()) {
                                initList.subList(offset, initList.size()).clear();
                                initList.addAll(offset, list);
                            }
                            if (type != Type.USERS) {
                                Storage.this.setFaveOffline(type, initList);
                            } else {
//                                Storage.this.setFaveUsersOffline((List<VKApiUserFull>) initList);
                            }
                            return initList;
                        } catch (JSONException e) {
                            Timber.e("Error:", e);
                            return new ArrayList<>();
                        }
                    });
        } else {
            return Observable.empty();
        }
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiPost>> getFavePostsOnline(int offset, boolean force) {
//        return this.<VKApiPost>getOnline(Type.POSTS, offset, force);
        // because fuck you that's why
        // and VkAttachments not serializing correctly
        long lastUpdateTime = preferences.getLong(LAST_POSTS_UPDATE_KEY, 0);
        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
            return VkObservable
                    .create(
                            new VKRequest(POSTS_URL)
                                    .addExtraParameter(VKApiConst.OFFSET, offset)
                                    .addExtraParameter(VKApiConst.COUNT, PaginationTool.DEFAULT_LIMIT)
                    )
                    .map((Func1<VKResponse, List<VKApiPost>>) vkResponse -> {
                        try {
                            preferences.putLong(LAST_POSTS_UPDATE_KEY, System.currentTimeMillis());

                            JSONObject o = new JSONObject(preferences.getString(POSTS_URL, DEFAULT_EMPTY_LIST));
                            JSONArray items = o.getJSONObject("response").getJSONArray("items");
                            Timber.v(items.length() + "");
                            while (items.length() > offset) {
                                items = JSONUtils.remove(offset, items);
                            }
                            Timber.v(items.length() + "");

                            JSONArray itemsIncome = vkResponse.json.getJSONObject("response").getJSONArray("items");
                            Timber.v(itemsIncome.length() + "");

                            for (int i = 0; i < itemsIncome.length(); i++) {
                                items.put(itemsIncome.get(i));
                            }

                            Timber.v(items.length() + "");

                            List<VKApiPost> initList = new VKPostArray().parse(o);
                            setFavePostsOffline(o.toString());

                            return initList;
                        } catch (JSONException e) {
                            Timber.e("Error:", e);
                            return new ArrayList<>();
                        }
                    });
        } else {
            return Observable.empty();
        }
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiLink>> getFaveLinksOnline(int offset, boolean force) {
        return this.<VKApiLink>getOnline(Type.LINKS, offset, force);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiPhoto>> getFavePhotosOnline(int offset, boolean force) {
        return this.<VKApiPhoto>getOnline(Type.PHOTOS, offset, force);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiVideo>> getFaveVideosOnline(int offset, boolean force) {
        return this.<VKApiVideo>getOnline(Type.VIDEOS, offset, force);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiUserFull>> getFaveUsersOnline(int offset, boolean force) {
        return this.<VKApiUserFull>getOnline(Type.USERS, offset, force)
                .map(list -> {
                    List<Integer> ids = new ArrayList<>();
                    for (VKApiUserFull userFull : list) {
                        ids.add(userFull.getId());
                    }
                    return ids;
                })
                .flatMap(this::getUsersInfo)
                .doOnNext(this::setFaveUsersOffline);
    }

    private <T> void setFaveOffline(Type type, final List<T> list) {
        JsonObject o = new JsonObject();
        JsonObject o1 = new JsonObject();
        o.add("response", o1);
        o1.add("count", gson.toJsonTree(list.size()));
        JsonArray o2 = new JsonArray();
        o1.add("items", o2);
        for (T t : list) {
            o2.add(gson.toJsonTree(t));
        }
        preferences.putString(type.getRequestUrl(), o.toString());
    }

    @Override
    public void setFaveUsersOffline(List<VKApiUserFull> users) {
        saveUsersOffline(users);
        preferences.putString(USERS_URL, gson.toJson(users));
    }

    @Override
    public void setFaveLinksOffline(final List<VKApiLink> links) {
        setFaveOffline(Type.LINKS, links);
    }

    @Override
    public void setFavePhotosOffline(final List<VKApiPhoto> photos) {
        setFaveOffline(Type.PHOTOS, photos);
    }

    @Override
    public void setFavePostsOffline(final List<VKApiPost> posts) {
        setFaveOffline(Type.POSTS, posts);
    }

    private void setFavePostsOffline(String response) {
        preferences.putString(POSTS_URL, response);
    }

    @Override
    public void setFaveVideosOffline(final List<VKApiVideo> videos) {
        setFaveOffline(Type.VIDEOS, videos);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiPost>> getFavePostsOffline() {
        return this.<VKApiPost>getFaveOffline(Type.POSTS);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiUserFull>> getFaveUsersOffline() {
        return Observable.just(gson.fromJson(preferences.getString(USERS_URL, "[]"), new TypeToken<List<VKApiUserFull>>() {
        }.getType()));
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiPhoto>> getFavePhotosOffline() {
        return this.<VKApiPhoto>getFaveOffline(Type.PHOTOS);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiVideo>> getFaveVideosOffline() {
        return this.<VKApiVideo>getFaveOffline(Type.VIDEOS);
    }

    @RxLogObservable
    @Override
    public Observable<List<VKApiLink>> getFaveLinksOffline() {
        return this.<VKApiLink>getFaveOffline(Type.LINKS);
    }

    @RxLogObservable
    private <T> Observable<List<T>> getFaveOffline(Type type) {
        try {
            return Observable.just((List<T>) type.getParser().parse(new JSONObject(preferences.getString(type.getRequestUrl(), DEFAULT_EMPTY_LIST))));
        } catch (JSONException e) {
            Timber.e("Error: ", e);
            preferences.putString(type.getRequestUrl(), null);
            return Observable.just(new ArrayList<>());
        }
    }

    @RxLogObservable
    @Override
    public Observable<Boolean> addFave(int id) {
        return VkObservable.create(new VKRequest("fave.addUser").addExtraParameter("user_id", id + ""))
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, VkStorageResponse.class))
                .map(vkStorageResponse -> vkStorageResponse.response == 1);
    }

    @RxLogObservable
    private Observable<Boolean> removeFromFave(String request, String field, String id) {
        return VkObservable.create(new VKRequest(request).addExtraParameter(field, id))
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, VkStorageResponse.class))
                .map(vkStorageResponse -> vkStorageResponse.response == 1);
    }

    @RxLogObservable
    @Override
    public Observable<Boolean> removeUserFromFave(int id) {
        return removeFromFave("fave.removeUser", "user_id", String.valueOf(id));
    }

    @RxLogObservable
    @Override
    public Observable<Boolean> removeLinkFromFave(final String id) {
        return removeFromFave("fave.removeLink", "link_id", id);
    }

    @RxLogObservable
    @Override
    public Observable<Integer> removeLike(String type, int ownerId, int id) {
        return VkObservable
                .create(
                        new VKRequest("likes.delete")
                                .addExtraParameter("type", type)
                                .addExtraParameter("item_id", id)
                                .addExtraParameter("owner_id", ownerId)
                )
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, VkLikesResponse.class))
                .map(vkStorageResponse -> vkStorageResponse.likes);
    }

    @Override
    public Observable<List<VKApiOwner>> getInfo(final List<Integer> ids) {
        return Observable.combineLatest(
                getUsersOffline(),
                getCommunitiesOffline(),
                (userFulls, vkApiCommunities) -> {
                    List<VKApiOwner> owners = new ArrayList<>();
                    for (VKApiUserFull user : userFulls) {
                        if (ids.contains(user.id)) {
                            owners.add(user);
                            ids.remove(Integer.valueOf(user.id));
                        }
                    }

                    for (VKApiCommunity community : vkApiCommunities) {
                        if (ids.contains(-community.id)) {
                            owners.add(community);
                            ids.remove(Integer.valueOf(-community.id));
                        }
                    }

                    List<Integer> users = new ArrayList<>();
                    List<Integer> groups = new ArrayList<>();

                    for (Integer id : ids) {
                        if (id > 0) {
                            users.add(id);
                        } else {
                            groups.add(-id);
                        }
                    }
                    return Observable.zip(
                            Observable.just(owners),
                            getUsersInfo(users).doOnNext(this::saveUsersOffline),
                            getCommunitiesInfo(groups).doOnNext(this::saveCommunitiesOffline),
                            (owners1, userFulls1, vkApiCommunities1) -> {
                                List<VKApiOwner> res = new ArrayList<>();
                                res.addAll(owners1);
                                res.addAll(vkApiCommunities1);
                                res.addAll(userFulls1);
                                return res;
                            }
                    ).toBlocking().first();
                }
        );
    }

    @RxLogObservable
    private Observable<List<VKApiUserFull>> getUsersOffline() {
        return Observable.just(gson.fromJson(preferences.getString(USERS, "[]"), new TypeToken<List<VKApiUserFull>>() {
        }.getType()));
    }

    @RxLogObservable
    private Observable<List<VKApiCommunity>> getCommunitiesInfo(final List<Integer> groupIds) {
        if (groupIds.isEmpty()) {
            return Observable.just(new ArrayList<>());
        }
        return VkObservable
                .create(VKApi.groups().getById(VKParameters.from("group_ids", TextUtils.join(",", groupIds))))
                .map((Func1<VKResponse, List<VKApiCommunity>>) vkResponse1 -> {
                    try {
                        List<VKApiCommunity> communities = new ArrayList<>();
                        List<VKApiCommunityFull> list = new VKApiCommunityArray().parse(vkResponse1.json).items;
                        communities.addAll(list);
                        return communities;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    return new ArrayList<>();
//                        return gson.fromJson(vkResponse1.responseString, CommunitiesFullResponse.class).response;
                });
    }

    @RxLogObservable
    private Observable<List<VKApiUserFull>> getUsersInfo(final List<Integer> userIds) {
        if (userIds.isEmpty()) {
            return Observable.just(new ArrayList<>());
        }
        return VkObservable
                .create(VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, TextUtils.join(",", userIds), VKApiConst.FIELDS, "photo_100, photo_200")))
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, UsersFullResponse.class).response);
    }

    @RxLogObservable
    private Observable<List<VKApiCommunity>> getCommunitiesOffline() {
        return Observable.just(gson.fromJson(preferences.getString(COMMUNITIES, "[]"), new TypeToken<List<VKApiCommunity>>() {
        }.getType()));
    }

    private void saveUsersOffline(List<VKApiUserFull> users) {
        if (users.isEmpty()) {
            return;
        }
        Set<VKApiUserFull> setUsers = new HashSet<>();
        List<VKApiUserFull> users1 = gson.fromJson(preferences.getString(USERS, "[]"), new TypeToken<List<VKApiUserFull>>() {
        }.getType());
        setUsers.addAll(users);
        setUsers.addAll(users1);
        List<VKApiUserFull> users2 = new ArrayList<>();
        users2.addAll(setUsers);
        preferences.putString(USERS, gson.toJson(users2));
    }

    private void saveCommunitiesOffline(List<VKApiCommunity> communities) {
        if (communities.isEmpty()) {
            return;
        }
        Set<VKApiCommunity> setCommunities = new HashSet<>();
        List<VKApiCommunity> communities1 = gson.fromJson(preferences.getString(COMMUNITIES, "[]"), new TypeToken<List<VKApiCommunity>>() {
        }.getType());
        setCommunities.addAll(communities);
        setCommunities.addAll(communities1);
        List<VKApiCommunity> communities2 = new ArrayList<>();
        communities2.addAll(setCommunities);
        preferences.putString(COMMUNITIES, gson.toJson(communities2));
    }

    @RxLogObservable
    @Override
    public Observable<TagsInfo> getTagsInfoOnline(boolean force) {
        long lastUpdateTime = preferences.getLong(LAST_TAGS_UPDATE_KEY, 0);
        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
            return VkObservable.create(
                    new VKRequest("storage.get").addExtraParameter("key", TAGS_INFO)
            )
                    .map(vkResponse -> {
                        preferences.putLong(LAST_TAGS_UPDATE_KEY, System.currentTimeMillis());
                        return gson.fromJson(vkResponse.responseString, VkStorageGetResponse.class).response;
                    })
                    .map(json -> {
                        TagsInfo tagsInfo;
                        if (json.isEmpty()) {
                            tagsInfo = new TagsInfo();
                        } else {
                            tagsInfo = gson.fromJson(json, TagsInfo.class);
                        }
                        preferences.putString(TAGS_INFO, json);
                        return tagsInfo;
                    });
        } else {
            return getTagsInfoOffline();
        }
    }

    @RxLogObservable
    @Override
    public Observable<TagsInfo> getTagsInfoOffline() {
        return Observable.just(getTagsInfoOfflineSync());
    }

    @RxLogObservable
    @Override
    public TagsInfo getTagsInfoOfflineSync() {
        TagsInfo tagsInfo = gson.fromJson(preferences.getString(TAGS_INFO, "{}"), TagsInfo.class);
        if (tagsInfo == null) {
            tagsInfo = new TagsInfo();
            saveTagsInfoOffline(tagsInfo);
        }
        return tagsInfo;
    }

    @RxLogObservable
    @Override
    public void saveTagsInfoOffline(TagsInfo tagsInfo) {
        preferences.putString(TAGS_INFO, gson.toJson(tagsInfo));
    }

    @RxLogObservable
    @Override
    public Observable<Boolean> saveTagsInfoOnline(TagsInfo tagsInfo) {
        saveTagsInfoOffline(tagsInfo);
        return VkObservable.create(new VKRequest("storage.set").addExtraParameter("key", TAGS_INFO).addExtraParameter("value", gson.toJson(tagsInfo)).addExtraParameter("global", 0))
                .map(vkResponse -> gson.fromJson(vkResponse.responseString, VkStorageResponse.class))
                .map(vkStorageResponse -> vkStorageResponse.response == 1)
                .flatMap(success -> {
                    if (success) {
                        return Observable.empty();
                    } else {
                        return Observable.error(new Throwable());
                    }
                });
    }

    public enum Type {
        USERS {
            @Override
            String getLastUpdateKey() {
                return LAST_USERS_UPDATE_KEY;
            }

            @Override
            String getRequestUrl() {
                return USERS_URL;
            }

            @Override
            VKList getParser() {
                return new VKUsersArray();
            }
        },
        PHOTOS {
            @Override
            String getLastUpdateKey() {
                return LAST_PHOTOS_UPDATE_KEY;
            }

            @Override
            String getRequestUrl() {
                return PHOTOS_URL;
            }

            @Override
            VKList getParser() {
                return new VKPhotoArray();
            }
        },
        VIDEOS {
            @Override
            String getLastUpdateKey() {
                return LAST_VIDEOS_UPDATE_KEY;
            }

            @Override
            String getRequestUrl() {
                return VIDEOS_URL;
            }

            @Override
            VKList getParser() {
                return new VKVideoArray();
            }
        },
        LINKS {
            @Override
            String getLastUpdateKey() {
                return LAST_LINKS_UPDATE_KEY;
            }

            @Override
            String getRequestUrl() {
                return LINKS_URL;
            }

            @Override
            VKList getParser() {
                return new VKLinkArray();
            }
        },
        POSTS {
            @Override
            String getLastUpdateKey() {
                return LAST_POSTS_UPDATE_KEY;
            }

            @Override
            String getRequestUrl() {
                return POSTS_URL;
            }

            @Override
            VKList getParser() {
                return new VKPostArray();
            }
        };

        abstract String getLastUpdateKey();

        abstract String getRequestUrl();

        abstract VKList getParser();
    }


//    @RxLogObservable
//    @Override
//    public Observable<List<VKApiPost>> getFavePostsOnline(int offset, boolean force) {
//        long lastUpdateTime = preferences.getLong(LAST_POSTS_UPDATE_KEY, 0);
//        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
//            return VkObservable.create(new VKRequest(POSTS_URL).addExtraParameter(VKApiConst.OFFSET, offset))
//                    .map(vkResponse -> {
//                        try {
//                            preferences.putLong(LAST_POSTS_UPDATE_KEY, System.currentTimeMillis());
//                            preferences.putString(POSTS_URL, vkResponse.responseString);
//                            return new VKPostArray().parse(vkResponse.json);
//                        } catch (JSONException e) {
//                            Timber.e("Error:", e);
//                            return new VKList<>();
//                        }
//                    });
//        } else {
//            return Observable.empty();
//        }
//    }

//    @RxLogObservable
//    @Override
//    public Observable<List<VKApiUserFull>> getFaveUsersOnline(int offset, boolean force) {
//        long lastUpdateTime = preferences.getLong(LAST_USERS_UPDATE_KEY, 0);
//        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
//            return VkObservable.create(new VKRequest(USERS_URL).addExtraParameter(VKApiConst.OFFSET, offset))
//                    .map((Func1<VKResponse, VKList<VKApiUserFull>>) vkResponse -> {
//                        preferences.putLong(LAST_USERS_UPDATE_KEY, System.currentTimeMillis());
//                        try {
//                            return new VKUsersArray().parse(vkResponse.json);
//                        } catch (JSONException e) {
//                            Timber.e("Error:", e);
//                            return new VKList<>();
//                        }
//                    })
//                    .map(list -> {
//                        StringBuilder builder = new StringBuilder();
//                        for (int i = 0; i < list.size(); i++) {
//                            builder.append(list.get(i).id);
//                            if (i != list.size() - 1) {
//                                builder.append(",");
//                            }
//                        }
//                        return builder.toString();
//                    })
//                    .flatMap(s -> VkObservable
//                            .create(VKApi.users().get(VKParameters.from(VKApiConst.USER_IDS, s, VKApiConst.FIELDS, "photo_200"))))
//                    .map(vkResponse -> gson.fromJson(vkResponse.responseString, UsersFullResponse.class).response)
//                    .doOnNext(users -> preferences.putString(USERS_URL, gson.toJson(users)));
//        } else {
//            return Observable.empty();
//        }
//    }

//    @RxLogObservable
//    @Override
//    public Observable<List<VKApiPhoto>> getFavePhotosOnline(int offset, boolean force) {
//        long lastUpdateTime = preferences.getLong(LAST_PHOTOS_UPDATE_KEY, 0);
//        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
//            return VkObservable.create(new VKRequest(PHOTOS_URL).addExtraParameter(VKApiConst.OFFSET, offset))
//                    .map(vkResponse -> {
//                        try {
//                            preferences.putLong(LAST_PHOTOS_UPDATE_KEY, System.currentTimeMillis());
//                            preferences.putString(PHOTOS_URL, vkResponse.responseString);
//                            return new VKPhotoArray().parse(vkResponse.json);
//                        } catch (JSONException e) {
//                            Timber.e("Error:", e);
//                            return new VKList<>();
//                        }
//                    });
//        } else {
//            return Observable.empty();
//        }
//    }
//
//    @RxLogObservable
//    @Override
//    public Observable<List<VKApiVideo>> getFaveVideosOnline(int offset, boolean force) {
//        long lastUpdateTime = preferences.getLong(LAST_VIDEOS_UPDATE_KEY, 0);
//        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
//            return VkObservable.create(new VKRequest(VIDEOS_URL).addExtraParameter(VKApiConst.OFFSET, offset))
//                    .map(vkResponse -> {
//                        try {
//                            preferences.putLong(LAST_VIDEOS_UPDATE_KEY, System.currentTimeMillis());
//                            preferences.putString(VIDEOS_URL, vkResponse.responseString);
//                            return new VKVideoArray().parse(vkResponse.json);
//                        } catch (JSONException e) {
//                            Timber.e("Error:", e);
//                            return new VKList<>();
//                        }
//                    });
//        } else {
//            return Observable.empty();
//        }
//    }

//    @RxLogObservable
//    @Override
//    public Observable<List<VKApiLink>> getFaveLinksOnline(int offset, boolean force) {
//        long lastUpdateTime = preferences.getLong(LAST_LINKS_UPDATE_KEY, 0);
//        if (force || lastUpdateTime == 0 || System.currentTimeMillis() - lastUpdateTime > 30 * 60 * 1000) {
//            return VkObservable.create(new VKRequest(LINKS_URL).addExtraParameter(VKApiConst.OFFSET, offset))
//                    .map(vkResponse -> {
//                        try {
//                            preferences.putLong(LAST_LINKS_UPDATE_KEY, System.currentTimeMillis());
//                            List<VKApiLink> links = new VKLinkArray().parse(vkResponse.json);
//                            preferences.putString(LINKS_URL, gson.toJson(links));
//                            return links;
//                        } catch (JSONException e) {
//                            Timber.e("Error:", e);
//                            return new VKList<>();
//                        }
//                    });
//        } else {
//            return Observable.empty();
//        }
//    }
}
