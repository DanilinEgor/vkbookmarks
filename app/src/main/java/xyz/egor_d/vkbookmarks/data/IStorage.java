package xyz.egor_d.vkbookmarks.data;

import com.fernandocejas.frodo.annotation.RxLogObservable;
import com.vk.sdk.api.model.VKApiLink;
import com.vk.sdk.api.model.VKApiOwner;
import com.vk.sdk.api.model.VKApiPhoto;
import com.vk.sdk.api.model.VKApiPost;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKApiVideo;

import java.util.List;

import rx.Observable;

public interface IStorage {
//    Observable<Boolean> saveInfoToStorageOnline(StorageInfo info);
//
//    Observable<StorageInfo> getInfoFromStorageOnline(boolean force);
//
//    Observable<StorageInfo> getInfoFromStorageOffline();
//
//    Observable<List<String>> getKeysFromStorage();

    @RxLogObservable
    Observable<List<VKApiPost>> getFavePostsOnline(int offset, boolean force);

    @RxLogObservable
    Observable<List<VKApiUserFull>> getFaveUsersOnline(int offset, boolean force);

    @RxLogObservable
    Observable<List<VKApiPhoto>> getFavePhotosOnline(int offset, boolean force);

    @RxLogObservable
    Observable<List<VKApiVideo>> getFaveVideosOnline(int offset, boolean force);

    @RxLogObservable
    Observable<List<VKApiLink>> getFaveLinksOnline(int offset, boolean force);

    @RxLogObservable
    void setFavePostsOffline(List<VKApiPost> posts);

    @RxLogObservable
    void setFaveVideosOffline(List<VKApiVideo> videos);

    @RxLogObservable
    Observable<List<VKApiPost>> getFavePostsOffline();

    @RxLogObservable
    Observable<List<VKApiUserFull>> getFaveUsersOffline();

    @RxLogObservable
    void setFaveUsersOffline(List<VKApiUserFull> users);

    @RxLogObservable
    void setFaveLinksOffline(List<VKApiLink> links);

    @RxLogObservable
    Observable<List<VKApiPhoto>> getFavePhotosOffline();

    @RxLogObservable
    Observable<List<VKApiVideo>> getFaveVideosOffline();

    @RxLogObservable
    Observable<List<VKApiLink>> getFaveLinksOffline();

    @RxLogObservable
    Observable<Boolean> addFave(int i);

    @RxLogObservable
    Observable<Boolean> removeUserFromFave(int id);

    @RxLogObservable
    Observable<Boolean> removeLinkFromFave(String id);

    @RxLogObservable
    void setFavePhotosOffline(List<VKApiPhoto> photos);

    @RxLogObservable
    Observable<Integer> removeLike(String type, int ownerId, int id);

    @RxLogObservable
    Observable<List<VKApiOwner>> getInfo(List<Integer> ids);

//    Observable<List<Tag>> getTagsInfoOnline(boolean force);

//    Observable<Boolean> saveTagsToStorageOnline(List<Tag> tags);

    @RxLogObservable
    Observable<TagsInfo> getTagsInfoOnline(boolean force);

    @RxLogObservable
    Observable<TagsInfo> getTagsInfoOffline();

    @RxLogObservable
    TagsInfo getTagsInfoOfflineSync();

    @RxLogObservable
    void saveTagsInfoOffline(TagsInfo tagsInfo);

    @RxLogObservable
    Observable<Boolean> saveTagsInfoOnline(TagsInfo tags);

//    List<Tag> getTagsOffline();
//
//    void setTagsOffline(List<Tag> tags);
//
//    @RxLogObservable
//    Observable<Map<Integer, List<Tag>>> getPostTagsOffline();
//
//    @RxLogObservable
//    void setPostTagsOffline(Map<Integer, List<Tag>> tags);
}
