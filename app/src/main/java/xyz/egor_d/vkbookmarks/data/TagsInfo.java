package xyz.egor_d.vkbookmarks.data;

import com.vk.sdk.api.model.Tag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class TagsInfo {
    public List<Tag> allTags = new ArrayList<>();
    public Map<Integer, List<Tag>> postTagsMap = new HashMap<>();
    public Map<Integer, List<Tag>> photoTagsMap = new HashMap<>();
    public Map<Integer, List<Tag>> videoTagsMap = new HashMap<>();
    public Map<Integer, List<Tag>> userTagsMap = new HashMap<>();
    public Map<Integer, List<Tag>> linkTagsMap = new HashMap<>();

    public void addToAllTags(List<Tag> tags) {
        Set<Tag> tagsSet = new TreeSet<>();
        tagsSet.addAll(tags);
        tagsSet.addAll(allTags);
        allTags.clear();
        allTags.addAll(tagsSet);
    }

    public void deleteTag(final Tag tag) {
        allTags.remove(tag);
        for (Integer i : postTagsMap.keySet()) {
            postTagsMap.get(i).remove(tag);
        }
        for (Integer i : photoTagsMap.keySet()) {
            photoTagsMap.get(i).remove(tag);
        }
        for (Integer i : videoTagsMap.keySet()) {
            videoTagsMap.get(i).remove(tag);
        }
        for (Integer i : userTagsMap.keySet()) {
            userTagsMap.get(i).remove(tag);
        }
        for (Integer i : linkTagsMap.keySet()) {
            linkTagsMap.get(i).remove(tag);
        }
    }

    // for changing name
    public void saveTag(final Tag tag) {
        allTags.remove(tag);
        allTags.add(tag);
        for (Integer i : postTagsMap.keySet()) {
            if (postTagsMap.get(i).remove(tag)) {
                postTagsMap.get(i).add(tag);
            }
        }
        for (Integer i : photoTagsMap.keySet()) {
            if (photoTagsMap.get(i).remove(tag)) {
                photoTagsMap.get(i).add(tag);
            }
        }
        for (Integer i : videoTagsMap.keySet()) {
            if (videoTagsMap.get(i).remove(tag)) {
                videoTagsMap.get(i).add(tag);
            }
        }
        for (Integer i : userTagsMap.keySet()) {
            if (userTagsMap.get(i).remove(tag)) {
                userTagsMap.get(i).add(tag);
            }
        }
        for (Integer i : linkTagsMap.keySet()) {
            if (linkTagsMap.get(i).remove(tag)) {
                linkTagsMap.get(i).add(tag);
            }
        }
    }
}
