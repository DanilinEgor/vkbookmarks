package xyz.egor_d.vkbookmarks.data;

import com.vk.sdk.api.model.VKApiUserFull;

import java.util.List;

public class UsersFullResponse {
    public List<VKApiUserFull> response;
}
