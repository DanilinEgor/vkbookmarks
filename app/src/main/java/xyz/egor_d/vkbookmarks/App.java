package xyz.egor_d.vkbookmarks;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;
import timber.log.Timber;
import xyz.egor_d.vkbookmarks.domain.DaggerMainComponent;
import xyz.egor_d.vkbookmarks.domain.MainComponent;
import xyz.egor_d.vkbookmarks.domain.StorageModule;

public class App extends Application {
    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
// VKAccessToken is invalid
            }
        }
    };

    MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this);
        Timber.plant(new Timber.DebugTree());
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        buildComponent();
    }

    private void buildComponent() {
        mainComponent = DaggerMainComponent.builder().storageModule(new StorageModule(this)).build();
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }

}
