package xyz.egor_d.vkbookmarks.domain;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Inject;

import xyz.egor_d.vkbookmarks.data.Storage;

public class Preferences {

    //    public static final String STORAGE_SYNCED = "storage_synced";
//    public static final String DELETED_SYNCED = "deleted_synced";
//    public static final String DELETED_IDS_KEY = "deleted_ids";
    private final SharedPreferences preferences;

    @Inject
    public Preferences(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void putString(final String key, final String value) {
        preferences.edit().putString(key, value).apply();
    }

    public void putLong(final String key, final long value) {
        preferences.edit().putLong(key, value).apply();
    }

    public void putBoolean(final String key, final boolean value) {
        preferences.edit().putBoolean(key, value).apply();
    }

    public long getLong(final String key, final long defaultValue) {
        return preferences.getLong(key, defaultValue);
    }

    public String getString(final String key, final String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    public boolean getBoolean(final String key, final boolean defaultValue) {
        return preferences.getBoolean(key, defaultValue);
    }

    public void clear() {
        putLong(Storage.LAST_POSTS_UPDATE_KEY, 0);
        putLong(Storage.LAST_VIDEOS_UPDATE_KEY, 0);
        putLong(Storage.LAST_PHOTOS_UPDATE_KEY, 0);
        putLong(Storage.LAST_LINKS_UPDATE_KEY, 0);
        putLong(Storage.LAST_USERS_UPDATE_KEY, 0);
        putLong(Storage.LAST_TAGS_UPDATE_KEY, 0);
        putString(Storage.POSTS_URL, null);
        putString(Storage.VIDEOS_URL, null);
        putString(Storage.PHOTOS_URL, null);
        putString(Storage.LINKS_URL, null);
        putString(Storage.USERS_URL, null);
        putString(Storage.COMMUNITIES, null);
        putString(Storage.USERS, null);
        putString(Storage.TAGS, null);
        putString(Storage.POST_TAGS, null);
        putString(Storage.TAGS_INFO, null);
    }
}