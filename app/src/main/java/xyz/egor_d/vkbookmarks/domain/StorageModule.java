package xyz.egor_d.vkbookmarks.domain;

import android.content.Context;

import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import xyz.egor_d.vkbookmarks.ExposeExclusionStrategy;
import xyz.egor_d.vkbookmarks.data.IStorage;
import xyz.egor_d.vkbookmarks.data.Storage;

@Module
public class StorageModule {
    Context c;

    public StorageModule(Context context) {
        c = context;
    }

    @Singleton
    @Provides
    Context provideContext() {
        return c;
    }

    @Singleton
    @Provides
    Preferences providePreferences(Context c) {
        return new Preferences(c);
    }

    @Singleton
    @Provides
    Gson provideGson() {
        ExclusionStrategy strategy = new ExposeExclusionStrategy();
        return new GsonBuilder()
                .serializeSpecialFloatingPointValues()
                .setExclusionStrategies(strategy)
                .create();
    }

    @Singleton
    @Provides
    IStorage getStorage(Preferences preferences, Gson gson) {
        return new Storage(preferences, gson);
    }
}
