package xyz.egor_d.vkbookmarks.domain;

import javax.inject.Singleton;

import dagger.Component;
import xyz.egor_d.vkbookmarks.presentation.FilterDialogActivity;
import xyz.egor_d.vkbookmarks.presentation.TagDialogActivity;
import xyz.egor_d.vkbookmarks.presentation.TagDialogFragment;
import xyz.egor_d.vkbookmarks.presentation.links.LinksFragment;
import xyz.egor_d.vkbookmarks.presentation.links.SimpleVkLinksFragment;
import xyz.egor_d.vkbookmarks.presentation.main.MainFragment;
import xyz.egor_d.vkbookmarks.presentation.photos.PhotosFragment;
import xyz.egor_d.vkbookmarks.presentation.photos.SimpleVkPhotosFragment;
import xyz.egor_d.vkbookmarks.presentation.posts.PostsFragment;
import xyz.egor_d.vkbookmarks.presentation.posts.SimpleVkPostsFragment;
import xyz.egor_d.vkbookmarks.presentation.users.SimpleVkUsersFragment;
import xyz.egor_d.vkbookmarks.presentation.users.UsersFragment;
import xyz.egor_d.vkbookmarks.presentation.videos.SimpleVkVideosFragment;
import xyz.egor_d.vkbookmarks.presentation.videos.VideosFragment;

@Singleton
@Component(modules = {StorageModule.class})
public interface MainComponent {
    void inject(MainFragment mainFragment);

    void inject(UsersFragment usersFragment);

    void inject(LinksFragment linksFragment);

    void inject(VideosFragment videosFragment);

    void inject(PostsFragment postsFragment);

    void inject(PhotosFragment photosFragment);

    void inject(SimpleVkUsersFragment simpleVkUsersFragment);

    void inject(SimpleVkPostsFragment simpleVkPostsFragment);

    void inject(SimpleVkPhotosFragment simpleVkPhotosFragment);

    void inject(SimpleVkLinksFragment simpleVkLinksFragment);

    void inject(SimpleVkVideosFragment simpleVkVideosFragment);

    void inject(TagDialogFragment tagDialogFragment);

    void inject(FilterDialogActivity filterDialogActivity);

    void inject(TagDialogActivity tagDialogActivity);
}
