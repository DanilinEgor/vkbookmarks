package xyz.egor_d.vkbookmarks;

import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.schedulers.Schedulers;
import rx.subscriptions.Subscriptions;

public class VkObservable {
    public static Observable<VKResponse> create(VKRequest request) {
        return Observable.create((Observable.OnSubscribe<VKResponse>) subscriber -> {
            subscriber.add(Subscriptions.create(request::cancel));
            request.executeWithListener(new VKRequest.VKRequestListener() {
                @Override
                public void onComplete(final VKResponse response) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(response);
                        subscriber.onCompleted();
                    }
                }

                @Override
                public void onError(final VKError error) {
                    if (!subscriber.isUnsubscribed()) {
                        switch (error.errorCode) {
                            case VKError.VK_API_ERROR:
                                subscriber.onError(new Throwable(error.apiError.errorMessage));
                                break;
                            default:
                                subscriber.onError(new Throwable(error.errorMessage));
                                break;
                        }
                    }
                }
            });
        })
                .subscribeOn(Schedulers.io());
//                .delay(2, TimeUnit.SECONDS);
    }


}
